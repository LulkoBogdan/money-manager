package com.bogdan.lulko.moneymanager.UI.fragments;

import android.content.res.AssetManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.adapters.SpinnerCurrenciesAdapter;
import com.bogdan.lulko.moneymanager.enums.EButtonTypes;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Dialogs;
import com.bogdan.lulko.moneymanager.helpers.JsonDecoder;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.interfaces.IDBWriteListener;
import com.bogdan.lulko.moneymanager.interfaces.ITwoButtonDialogCallback;
import com.bogdan.lulko.moneymanager.models.BalanceCurrency;
import com.bogdan.lulko.moneymanager.models.BalanceItem;
import com.bogdan.lulko.moneymanager.models.CountryList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class AddNewBalanceFragment extends ABaseFragment {

    //TODO set max length for balance name and balance value
    private TextInputLayout tilBalanceName;
    private EditText etBalanceName, etBalanceDescription, etStartValue;
    private Switch swSynchronize;

    private SpinnerCurrenciesAdapter adapter;
    private List<BalanceCurrency> currencies = new ArrayList<>();
    private CountryList countryList;

    private int spinnerSelectedPosition = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_new_balance, container, false);
        initView(rootView);
        checkCountOfBalances(false);
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView(View rootView){
        initToolbar(R.id.app_bar, R.string.new_balance_toolbar_title, R.mipmap.ic_arrow_back_black_36dp, rootView, true);
        Dialogs.showLoadingProgressDialog(getActivity());
        currencies.clear();
        currencies.add(new BalanceCurrency(BalanceCurrency.NONE(getActivity())));
        initCurrencies();
        Button btnSave = (Button) rootView.findViewById(R.id.buttonSave);
        Button btnCancel = (Button) rootView.findViewById(R.id.buttonCancel);
        initRectangleButton(btnSave, EButtonTypes.YELLOW_LIGHT);
        initRectangleButton(btnCancel, EButtonTypes.GRAY);
        btnCancel.setOnClickListener(actionButtonListener);
        btnSave.setOnClickListener(actionButtonListener);
        Spinner spCurrencies = (Spinner) rootView.findViewById(R.id.spinnerCurrency);
        adapter = new SpinnerCurrenciesAdapter(getActivity(), R.layout.adapter_item_spinner_currency, currencies);
        adapter.setDropDownViewResource(R.layout.adapter_item_spinner_dropdown_currency);
        spCurrencies.setAdapter(adapter);
        spCurrencies.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerSelectedPosition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tilBalanceName = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutBalanceName);
        tilBalanceName.setErrorEnabled(true);
        ((TextInputLayout)rootView.findViewById(R.id.textInputLayoutBalanceDescription)).setErrorEnabled(true);
        etBalanceName = (EditText) rootView.findViewById(R.id.editTextBalanceName);
        etBalanceDescription = (EditText) rootView.findViewById(R.id.editTextBalanceDescription);
        etStartValue = (EditText) rootView.findViewById(R.id.editTextStartValue);
        etStartValue.setText("0");
        swSynchronize = (Switch) rootView.findViewById(R.id.switchSynchronize);
    }

    private View.OnClickListener actionButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.buttonCancel:
                    getFragmentManager().popBackStack();
                    break;
                case R.id.buttonSave:
                    checkCountOfBalances(true);
                    break;
            }
        }
    };

    private IDBWriteListener writeListener = new IDBWriteListener() {
        @Override
        public void onWriteFinish(long count) {
            Dialogs.hideDialog();
            if(count > 0){
                String balanceName = etBalanceName.getText().toString();
                setSelectedBalanceItem(getDBHelper().isBalanceNameAvailable(balanceName));
                getFragmentManager().popBackStack();
            }else{
                Dialogs.showCustomOneButtonDialog(getActivity(), R.string.btn_label_ok, R.string.new_balance_message_balance_was_not_created, null, true);
            }
        }
    };

    public HashMap<String, BalanceCurrency> getAllCurrencies(){
        countryList = getCountryList();
        HashMap<String, BalanceCurrency> currencies = new HashMap<>();
        Locale[] locales = Locale.getAvailableLocales();
        for(Locale loc : locales) {
            try {
                BalanceCurrency currency = new BalanceCurrency();
                currency.setCurrencySymbol(Currency.getInstance(loc).getSymbol());
                currency.setCurrencyCode(Currency.getInstance(loc).getCurrencyCode());
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
                    currency.setCurrencyName(Currency.getInstance(loc).getDisplayName());
                else
                    currency.setCurrencyName(Currency.getInstance(loc).getCurrencyCode());
                HashMap<String, CountryList.Country> countryMap = countryList.getCountryMap();
                if(countryMap.isEmpty() || !countryMap.containsKey(loc.getCountry())) {
                    currency.setCountryName(loc.getCountry());
                }else{
                    currency.setCountryName(countryMap.get(loc.getCountry()).getCountry());
                }
                currency.setCountryCode(loc.getISO3Country());
                currencies.put(currency.getCountryName(), currency);
            } catch(Exception exc){
                if(Utils.DEBUG)
                    exc.printStackTrace();
            }
        }
        return currencies;
    }

    public CountryList getCountryList() {
        if (countryList == null){
            InputStream is = null;
            try {
                AssetManager manager = getActivity().getAssets();
                is = manager.open("countries.json");
                countryList = JsonDecoder.decodeCountries(is);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return countryList;
    }

    private void initCurrencies(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                currencies.addAll(getAllCurrencies().values());
                Collections.sort(currencies, new Comparator<BalanceCurrency>() {
                    @Override
                    public int compare(BalanceCurrency lhs, BalanceCurrency rhs) {
                        if (lhs.getCountryName().equalsIgnoreCase(BalanceCurrency.NONE(getActivity()))) {
                            return -1;
                        } else if (rhs.getCountryName().equalsIgnoreCase(BalanceCurrency.NONE(getActivity()))) {
                            return 1;
                        }
                        return lhs.getCountryName().compareToIgnoreCase(rhs.getCountryName());
                    }
                });
                Dialogs.hideDialog();
            }
        }).start();
    }

    private void checkCountOfBalances(final boolean save){
        getDBHelper().asyncGetAllBalances(new IDBReadListener<BalanceItem>() {
            @Override
            public void onReadFinish(List<BalanceItem> items) {
                if (items != null && items.size() == Constants.Common.MAX_BALANCE_COUNT) {
                    String message = new Formatter().format(getString(R.string.new_balance_message_max_balance_count_reached), Constants.Common.MAX_BALANCE_COUNT).toString();
                    Dialogs.showCustomTwoButtonDialog(getActivity(), R.string.btn_label_continue, R.string.btn_label_cancel, message, new ITwoButtonDialogCallback() {
                        @Override
                        public void onLeftButtonClick() {}

                        @Override
                        public void onRightButtonClick() {
                            getFragmentManager().popBackStack();
                        }

                        @Override
                        public void onCancel() {}
                    }, true);
                } else if(save){
                    saveNewBalanceItem();
                }
            }
            @Override
            public void onReadFinish(Map<Long, BalanceItem> items) {}
        });
    }

    private void saveNewBalanceItem(){
        Dialogs.showLoadingProgressDialog(getActivity());
        String balanceName = etBalanceName.getText().toString();
        if(TextUtils.isEmpty(balanceName)){
            tilBalanceName.setError(getString(R.string.new_balance_message_name_required));
            Dialogs.hideDialog();
            return;
        }
        if(getDBHelper().isBalanceNameAvailable(balanceName) != Utils.NO_VALUE){
            tilBalanceName.setError(getString(R.string.new_balance_message_name_already_in_use));
            Dialogs.hideDialog();
            return;
        }
        tilBalanceName.setError("");
        BalanceItem balanceItem = new BalanceItem();
        balanceItem.setCurrencySymbol(spinnerSelectedPosition == 0 ? "" : adapter.getItem(spinnerSelectedPosition).getCurrencySymbol());
        balanceItem.setName(balanceName);
        balanceItem.setDescription(etBalanceDescription.getText().toString());
        balanceItem.setValue(Double.parseDouble(etStartValue.getText().toString()));
        balanceItem.setStartValue(balanceItem.getValue());
        balanceItem.setEarnings(0);
        balanceItem.setCosts(0);
        balanceItem.setPlannedEarnings(0);
        balanceItem.setPlannedCosts(0);
        balanceItem.setSynchronizable(swSynchronize.isChecked());
        balanceItem.setCreatedAt(System.currentTimeMillis() / 1000);
        balanceItem.setUpdatedAt(System.currentTimeMillis() / 1000);
        balanceItem.setCreatedByUser("Local");//TODO change to user name when synchronizable
        balanceItem.setUpdatedByUser(balanceItem.getCreatedByUser());
        balanceItem.setKey(System.currentTimeMillis());
        getDBHelper().asyncAddBalanceItem(balanceItem, writeListener);
    }
}
