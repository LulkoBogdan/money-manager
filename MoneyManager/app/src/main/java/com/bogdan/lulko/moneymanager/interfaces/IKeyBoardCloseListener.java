package com.bogdan.lulko.moneymanager.interfaces;

public interface IKeyBoardCloseListener {
    void onKeyBoardClose();
}
