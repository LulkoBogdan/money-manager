package com.bogdan.lulko.moneymanager.interfaces;

public interface IDBWriteListener {
    void onWriteFinish(long count);
}
