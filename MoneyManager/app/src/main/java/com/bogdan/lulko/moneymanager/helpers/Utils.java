package com.bogdan.lulko.moneymanager.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class Utils {

    public static final int NO_VALUE = -1;
    private static final String TAG = "MoneyManager";

    public final static boolean DEBUG = true;

    public static void log(String msg){
        if (DEBUG){
            Log.d(TAG, getLocation() + msg);
        }
    }

    private static String getLocation() {
        final String className = Utils.class.getName();
        final StackTraceElement[] traces = Thread.currentThread().getStackTrace();
        boolean found = false;
        for (StackTraceElement trace : traces) {
            try {
                if (found) {
                    if (!trace.getClassName().startsWith(className)) {
                        Class<?> clazz = Class.forName(trace.getClassName());
                        return "[" + getClassName(clazz) + ":" + trace.getMethodName() + ":" + trace.getLineNumber() + "]: ";
                    }
                } else if (trace.getClassName().startsWith(className)) {
                    found = true;
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        return "[]: ";
    }

    private static String getClassName(Class<?> clazz) {
        if (clazz != null) {
            if (!TextUtils.isEmpty(clazz.getSimpleName())) {
                return clazz.getSimpleName();
            }
            return getClassName(clazz.getEnclosingClass());
        }
        return "";
    }

    public static boolean isConnectedToNetwork(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static Typeface getTitlesTypeface(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/BodoniCondC.ttf");
    }

    public static Typeface getCommonTypeface(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueCyr-Roman.ttf");
    }

    public static Typeface getCommonBoldTypeface(Context context){
        return Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaBold.ttf");
    }

    public static String getStringFromPrefs(String key, String defaultValue, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getString(key, defaultValue);
    }

    public static void putStringToPrefs(String key, String value, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit()
                .putString(key, value)
                .apply();
    }

    public static int getIntFromPrefs(String key, int defaultValue, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getInt(key, defaultValue);
    }

    public static void putIntToPrefs(String key, int value, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit()
                .putInt(key, value)
                .apply();
    }

    public static boolean getBooleanFromPrefs(String key, boolean defaultValue, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(key, defaultValue);
    }

    public static void putBooleanToPrefs(String key, boolean value, Context context){
        SharedPreferences preferences = context.getSharedPreferences(Constants.Common.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        preferences.edit()
                .putBoolean(key, value)
                .apply();
    }

    public static String millisecondsToDate(long milliseconds, String pattern){
        String output;
        SimpleDateFormat formatter = new SimpleDateFormat(pattern, Locale.getDefault());
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        output = formatter.format(calendar.getTime());
        return output;
    }

    public static long getMillisecondsFromString(String input, String pattern){
        long time = 0;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.getDefault());
            Date date = sdf.parse(input);
            time = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time;
    }
}
