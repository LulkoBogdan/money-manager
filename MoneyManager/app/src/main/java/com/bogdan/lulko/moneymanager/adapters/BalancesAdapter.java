package com.bogdan.lulko.moneymanager.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.UI.activities.MainActivity;
import com.bogdan.lulko.moneymanager.UI.fragments.InfoBalanceFragment;
import com.bogdan.lulko.moneymanager.helpers.FormatterHelper;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

import java.util.List;

import codetail.graphics.drawables.DrawableHotspotTouch;
import codetail.graphics.drawables.LollipopDrawable;
import codetail.graphics.drawables.LollipopDrawablesCompat;

public class BalancesAdapter extends RecyclerView.Adapter<BalancesAdapter.ViewHolder>{

    private List<BalanceItem> balances;
    private Context context;

    public BalancesAdapter(List<BalanceItem> balances, Context context){
        this.balances = balances;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_item_card_balance, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BalanceItem item = balances.get(position);
        holder.tvName.setText(item.getName());
        holder.tvSynchronized.setVisibility(item.isSynchronizable() ? View.VISIBLE : View.GONE);
        holder.tvUpdatedAt.setText(Utils.millisecondsToDate(item.getUpdatedAt() * 1000, "dd MMM yyyy"));
        holder.tvCreatedAt.setText(Utils.millisecondsToDate(item.getCreatedAt() * 1000, "dd MMM yyyy"));
        holder.tvCreatedBy.setText(item.getCreatedByUser());
        holder.tvValue.setText(FormatterHelper.formatBalanceValueString(item));
        holder.tvUpdatedBy.setText(item.getUpdatedByUser());
        if(item.getValue() < 0){
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.text_color_costs));
        }else if(item.getValue() > 0){
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.text_color_earnings));
        }else{
            holder.tvValue.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));
        }
        if(TextUtils.isEmpty(item.getDescription())){
            if(holder.tvDescriptionLabel.getVisibility() == View.VISIBLE || holder.tvDescription.getVisibility() == View.VISIBLE) {
                holder.tvDescription.setVisibility(View.GONE);
                holder.tvDescriptionLabel.setVisibility(View.GONE);
            }
        }else{
            holder.tvDescription.setText(item.getDescription());
            if(holder.tvDescriptionLabel.getVisibility() == View.GONE || holder.tvDescription.getVisibility() == View.GONE) {
                holder.tvDescription.setVisibility(View.VISIBLE);
                holder.tvDescriptionLabel.setVisibility(View.VISIBLE);
            }
        }
        BalanceItem selectedBalanceItem = ((MainActivity) context).getSelectedBalanceItem();
        if(selectedBalanceItem != null && selectedBalanceItem.getId() == item.getId()){
            holder.btnCurrent.setImageResource(R.mipmap.ic_radio_button_checked_black_24dp);
        }else{
            holder.btnCurrent.setImageResource(R.mipmap.ic_radio_button_unchecked_black_24dp);
        }
        if(item.getCreatedAt() != item.getUpdatedAt()){
            holder.tvLabelUpdatedBy.setVisibility(View.VISIBLE);
            holder.tvLabelUpdatedAt.setVisibility(View.VISIBLE);
            holder.tvUpdatedBy.setVisibility(View.VISIBLE);
            holder.tvUpdatedAt.setVisibility(View.VISIBLE);
        }else{
            holder.tvLabelUpdatedBy.setVisibility(View.GONE);
            holder.tvLabelUpdatedAt.setVisibility(View.GONE);
            holder.tvUpdatedBy.setVisibility(View.GONE);
            holder.tvUpdatedAt.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return balances.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvName;
        TextView tvSynchronized;
        TextView tvValue;
        TextView tvCreatedBy;
        TextView tvCreatedAt;
        TextView tvUpdatedAt;
        TextView tvDescription;
        TextView tvDescriptionLabel;
        TextView tvUpdatedBy;
        TextView tvLabelUpdatedAt;
        TextView tvLabelUpdatedBy;
        ImageButton btnInfo;
        ImageButton btnCurrent;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = (TextView) itemView.findViewById(R.id.textViewBalanceName);
            tvSynchronized = (TextView) itemView.findViewById(R.id.textViewSynchronized);
            tvValue = (TextView) itemView.findViewById(R.id.textViewValue);
            tvCreatedAt = (TextView) itemView.findViewById(R.id.textViewCreatedAt);
            tvCreatedBy = (TextView) itemView.findViewById(R.id.textViewCreatedBy);
            tvUpdatedAt = (TextView) itemView.findViewById(R.id.textViewUpdatedAt);
            tvDescription = (TextView) itemView.findViewById(R.id.textViewDescription);
            tvDescriptionLabel = (TextView) itemView.findViewById(R.id.textViewLabelDescription);
            tvUpdatedBy = (TextView) itemView.findViewById(R.id.textViewUpdatedBy);
            tvLabelUpdatedAt = (TextView) itemView.findViewById(R.id.textViewLabelUpdatedAt);
            tvLabelUpdatedBy = (TextView) itemView.findViewById(R.id.textViewLabelUpdatedBy);
            btnInfo = (ImageButton) itemView.findViewById(R.id.buttonInfo);
            initOvalButton(btnInfo);
            btnInfo.setOnClickListener(this);
            btnCurrent = (ImageButton) itemView.findViewById(R.id.buttonCurrent);
            initOvalButton(btnCurrent);
            btnCurrent.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(v.getId() == btnInfo.getId()) {
                ((MainActivity) context).changeFragment(InfoBalanceFragment.getInstance(balances.get(getAdapterPosition())), true, false, false);
            }else{
                ((MainActivity) context).setSelectedBalance(balances.get(getAdapterPosition()));
                notifyDataSetChanged();
            }
        }
    }

    public void initOvalButton(View button){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return;
        int resId = R.drawable.ripple_oval;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            button.setBackground(getDrawableRipple(resId));
        }else{
            button.setBackgroundDrawable(getDrawableRipple(resId));
        }
        button.setClickable(true);
        button.setOnTouchListener(new DrawableHotspotTouch((LollipopDrawable) button.getBackground()));
    }

    public Drawable getDrawableRipple(int id){
        return LollipopDrawablesCompat.getDrawable(context.getResources(), id, context.getTheme());
    }
}
