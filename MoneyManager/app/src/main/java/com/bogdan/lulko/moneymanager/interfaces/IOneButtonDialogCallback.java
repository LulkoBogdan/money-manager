package com.bogdan.lulko.moneymanager.interfaces;

public interface IOneButtonDialogCallback {
    void onButtonClick();
    void onCancel();
}
