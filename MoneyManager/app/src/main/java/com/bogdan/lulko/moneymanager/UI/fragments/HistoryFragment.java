package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.adapters.HistoryAdapter;
import com.bogdan.lulko.moneymanager.enums.EDashboardPages;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.DBHelper;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.models.HistoryItem;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Map;

public class HistoryFragment extends ABaseFragment {

    private HistoryAdapter adapter;
    private List<HistoryItem> historyItems = new ArrayList<>();

    private EDashboardPages currentPage = EDashboardPages.ALL;

    public static HistoryFragment getInstance(EDashboardPages dashboardPage){
        HistoryFragment fragment = new HistoryFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BundleKeys.KEY_PAGE_TYPE_ID, dashboardPage.getId());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initPageType();
        View rootView = inflater.inflate(R.layout.fragment_all_history, container, false);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView){
        ListView lvHistory = (ListView) rootView.findViewById(R.id.listViewHistory);
        adapter = new HistoryAdapter(getActivity(), R.layout.adapter_item_history, historyItems);
        lvHistory.setAdapter(adapter);
        TextView emptyTextView = (TextView) rootView.findViewById(R.id.textViewEmptyView);
        emptyTextView.setText(new Formatter().format(getString(R.string.dashboard_label_empty_list), currentPage.getEmptyViewText(getActivity())).toString());
        lvHistory.setEmptyView(emptyTextView);
        if(getSelectedBalanceItem() != null)
            readHistoryFromDB();
    }

    private void initPageType(){
        Bundle arguments = getArguments();
        if(arguments != null){
            currentPage = EDashboardPages.getPageById(arguments.getInt(Constants.BundleKeys.KEY_PAGE_TYPE_ID));
        }
    }

    public void updateList(){
        if(!isAdded() || getActivity() == null || adapter == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void readHistoryFromDB(){
        //TODO add loading from server
        DBHelper dbHelper = getDBHelper();
        if(dbHelper != null){
            dbHelper.asyncGetHistory(currentPage, getSelectedBalanceItem(), new IDBReadListener<HistoryItem>() {
                @Override
                public void onReadFinish(List<HistoryItem> items) {
                    if(adapter != null)
                        adapter.notifyDataSetChanged();
                }
                @Override
                public void onReadFinish(Map<Long, HistoryItem> items) {}
            });
        }
    }
}
