package com.bogdan.lulko.moneymanager.helpers;

import com.bogdan.lulko.moneymanager.models.BalanceItem;

import java.text.DecimalFormat;

public class FormatterHelper {

    public static String formatBalanceValueString(BalanceItem balanceItem){
        return new DecimalFormat("#.##").format(balanceItem.getValue()) + " " + balanceItem.getCurrencySymbol();
    }

}
