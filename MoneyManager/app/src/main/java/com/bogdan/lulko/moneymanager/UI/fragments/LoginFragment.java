package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.UI.views.EditTextKeyBoardCloseEvent;
import com.bogdan.lulko.moneymanager.enums.EButtonTypes;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IKeyBoardCloseListener;

import java.lang.reflect.Field;

public class LoginFragment extends ABaseFragment{

    private EditTextKeyBoardCloseEvent etLogin, etPassword;
    private TextInputLayout tilLogin, tilPassword;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Button btnLogin, btnSignUp;
    private TextView tvInternetErrorMessage;

    private boolean initialInternetCheck = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);
        initViews(rootView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        checkInternetConnection();
    }

    private void initViews(View rootView){
        Button btnSkip;
        btnSkip = (Button) rootView.findViewById(R.id.buttonSkip);
        initRectangleButton(btnSkip, EButtonTypes.GRAY);
        btnLogin = (Button) rootView.findViewById(R.id.buttonLogin);
        initRectangleButton(btnLogin, EButtonTypes.YELLOW_LIGHT);
        btnSignUp = (Button) rootView.findViewById(R.id.buttonSignUp);
        initRectangleButton(btnSignUp, EButtonTypes.YELLOW_LIGHT);
        btnSkip.setOnClickListener(buttonsAction);
        btnLogin.setOnClickListener(buttonsAction);
        btnSignUp.setOnClickListener(buttonsAction);
        collapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(getString(R.string.app_name));
        makeCollapsingToolbarLayoutLooksGood(collapsingToolbarLayout);
        etLogin = (EditTextKeyBoardCloseEvent) rootView.findViewById(R.id.editTextLogin);
        etPassword = (EditTextKeyBoardCloseEvent) rootView.findViewById(R.id.editTextPassword);
        etPassword.setKeyBoardCloseListener(keyBoardCloseListener);
        etLogin.setKeyBoardCloseListener(keyBoardCloseListener);
        tilLogin = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutLogin);
        tilPassword = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutPassword);
        tilLogin.setErrorEnabled(true);
        tilPassword.setErrorEnabled(true);
        tvInternetErrorMessage = (TextView) rootView.findViewById(R.id.textViewInternetErrorMessage);
   }

    private View.OnClickListener buttonsAction = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.buttonSkip:
                    hideKeyBoard();
                    if(getView() != null) {
                        View rootView = getView().findViewById(R.id.coordinatorLayout);
                        Snackbar.make(rootView, R.string.login_snackbar_skip_message, Snackbar.LENGTH_LONG)
                                .setAction(R.string.btn_label_continue, snackbarAction)
                                .setActionTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary))
                                .show();
                    }
                    break;
                case R.id.buttonSignUp:
                    changeFragment(new SignUpFragment(), true, false, true);
                    break;
                case R.id.buttonLogin:
                    login();
                    break;
            }
        }
    };

    private View.OnClickListener snackbarAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Utils.putBooleanToPrefs(Constants.PrefKeys.KEY_IS_LOGGED_IN, true, getActivity());
            changeFragment(new DashboardFragment(), false, true, true);
        }
    };

    private void login(){
        String login = etLogin.getText().toString();
        if(TextUtils.isEmpty(login)){
            tilLogin.setError(getString(R.string.login_hint_error_login_required));
            return;
        }
        tilLogin.setError("");
        String password = etPassword.getText().toString();
        if(TextUtils.isEmpty(password)){
            tilPassword.setError(getString(R.string.login_hint_error_password_required));
            return;
        }
        tilPassword.setError("");
        //TODO login
    }

    private void makeCollapsingToolbarLayoutLooksGood(CollapsingToolbarLayout collapsingToolbarLayout) {
        try {
            final Field field = collapsingToolbarLayout.getClass().getDeclaredField("mCollapsingTextHelper");
            field.setAccessible(true);
            final Object object = field.get(collapsingToolbarLayout);
            final Field tpf = object.getClass().getDeclaredField("mTextPaint");
            tpf.setAccessible(true);
            ((TextPaint) tpf.get(object)).setTypeface(Utils.getTitlesTypeface(getActivity()));
        } catch (Exception ignored) {
        }
    }

    private IKeyBoardCloseListener keyBoardCloseListener = new IKeyBoardCloseListener() {
        @Override
        public void onKeyBoardClose() {
            Utils.log("QWERTY message in fragment");
            if(collapsingToolbarLayout != null) {
                Utils.log("QWERTY HERE");
                //TODO wait until will be fixed disappearing of title
                collapsingToolbarLayout.setTitleEnabled(true);
                collapsingToolbarLayout.setTitle(getString(R.string.app_name));
                collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedTitle);
                collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedTitle);
            }
        }
    };

    private void checkInternetConnection(){
        if(Utils.isConnectedToNetwork(getActivity())){
            btnSignUp.setEnabled(true);
            btnLogin.setEnabled(true);
            tvInternetErrorMessage.setVisibility(View.GONE);
        }else{
            btnSignUp.setEnabled(false);
            btnLogin.setEnabled(false);
            tvInternetErrorMessage.setVisibility(View.VISIBLE);
            if(initialInternetCheck && getView() != null){
                View rootView = getView().findViewById(R.id.coordinatorLayout);
                Snackbar.make(rootView, R.string.login_snackbar_no_internet_connection, Snackbar.LENGTH_SHORT)
                        .setAction(R.string.btn_label_ok, null)
                        .show();
                initialInternetCheck = false;
            }
        }
    }
}
