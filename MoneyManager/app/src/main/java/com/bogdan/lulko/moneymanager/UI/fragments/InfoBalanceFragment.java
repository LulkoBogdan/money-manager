package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

public class InfoBalanceFragment extends ABaseFragment{

    public static InfoBalanceFragment getInstance(BalanceItem balanceItem){
        InfoBalanceFragment fragment = new InfoBalanceFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKeys.KEY_BALANCE_ITEM_TO_EDIT, balanceItem);
        fragment.setArguments(bundle);
        return fragment;
    }

    private BalanceItem balanceItem;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDataFromArguments();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getDataFromArguments(){
        if(getArguments() != null){
            balanceItem = getArguments().getParcelable(Constants.BundleKeys.KEY_BALANCE_ITEM_TO_EDIT);
        }
    }

}
