package com.bogdan.lulko.moneymanager.models;

import java.util.HashMap;
import java.util.List;

public class CountryList{

    public List<Country> countries;

    public CountryList() {

    }

    public String[] getCountries(){
        String[] arr = new String[countries.size()];
        int i = 0;
        for(Country country: countries){
            arr[i++] = country.getCountry();
        }
        return arr;
    }

    public String[] getISO2Countries(){
        String[] arr = new String[countries.size()];
        int i = 0;
        for(Country country: countries){
            arr[i++] = country.getCountryIso();
        }
        return arr;
    }

    public HashMap<String, Country> getCountryMap(){
        HashMap<String, Country> result = new HashMap<String, Country>();
        for(Country country: countries){
            result.put(country.getCountryIso(), country);
        }
        return result;
    }

    public List<Country> getListCountries(){
        return countries;
    }

    public int getSize(){
        return countries.size();
    }

    public String getISO2Country(int position){
        return countries.get(position).getCountryIso();
    }

    public static class Country {

        public String country;
        public String countryISO;

        public Country() {

        }

        public Country(String country, String countryIso) {
            this.country = country;
            this.countryISO = countryIso;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCountryIso() {
            return countryISO;
        }

        public void setCountryIso(String countryIso) {
            this.countryISO = countryIso;
        }


    }

}
