package com.bogdan.lulko.moneymanager.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.models.HistoryItem;

import java.util.List;

public class HistoryAdapter extends ArrayAdapter<HistoryItem>{

    private int resId;
    private int earnings, costs, plannedEarnings, plannedCosts;

    public HistoryAdapter(Context context, int resource, List<HistoryItem> objects) {
        super(context, resource, objects);
        this.resId = resource;
        earnings = ContextCompat.getColor(context, R.color.text_color_earnings);
        costs = ContextCompat.getColor(context, R.color.text_color_costs);
        plannedEarnings = ContextCompat.getColor(context, R.color.text_color_planned_earnings);
        plannedCosts = ContextCompat.getColor(context, R.color.text_color_planned_costs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(resId, parent, false);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        HistoryItem item = getItem(position);

        return super.getView(position, convertView, parent);
    }

    private class ViewHolder{

    }
}
