package com.bogdan.lulko.moneymanager.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bogdan.lulko.moneymanager.enums.EDashboardPages;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.interfaces.IDBWriteListener;
import com.bogdan.lulko.moneymanager.models.BalanceItem;
import com.bogdan.lulko.moneymanager.models.HistoryItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class DBHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "money_manager_db";
    private static final int DB_VERSION = 3;

    private static final String TABLE_NAME_BALANCES = "table_balances";
    private static final String TABLE_NAME_HISTORY = "table_history";

    private static final String TABLE_COLUMN_BALANCE_ID = "id";
    private static final String TABLE_COLUMN_BALANCE_NAME = "balance_name";
    private static final String TABLE_COLUMN_BALANCE_IS_SYNCHRONIZABLE = "balance_is_synchronizable";
    private static final String TABLE_COLUMN_BALANCE_VALUE = "balance_value";
    private static final String TABLE_COLUMN_BALANCE_CREATED_BY_USER = "balance_created_by_user";
    private static final String TABLE_COLUMN_BALANCE_CREATED_AT = "balance_created_at";
    private static final String TABLE_COLUMN_BALANCE_LAST_UPDATED_AT = "balance_last_updated_at";
    private static final String TABLE_COLUMN_BALANCE_EARNINGS = "balance_earnings";
    private static final String TABLE_COLUMN_BALANCE_COSTS = "balance_costs";
    private static final String TABLE_COLUMN_BALANCE_PLANNED_EARNINGS = "balance_planned_earnings";
    private static final String TABLE_COLUMN_BALANCE_PLANNED_COSTS = "balance_planned_costs";
    private static final String TABLE_COLUMN_BALANCE_CURRENCY_SYMBOL = "balance_currency_symbol";
    private static final String TABLE_COLUMN_BALANCE_DESCRIPTION = "balance_description";
    private static final String TABLE_COLUMN_BALANCE_START_VALUE = "balance_start_value";
    private static final String TABLE_COLUMN_BALANCE_KEY = "balance_key";
    private static final String TABLE_COLUMN_BALANCE_UPDATED_BY_USER = "balance_updated_by_user";

    private static final String TABLE_COLUMN_HISTORY_ID = "id";
    private static final String TABLE_COLUMN_HISTORY_BALANCE_ID = "balance_id";
    private static final String TABLE_COLUMN_HISTORY_TYPE_ID = "history_type_id";
    private static final String TABLE_COLUMN_HISTORY_DESCRIPTION = "history_description";
    private static final String TABLE_COLUMN_HISTORY_VALUE = "history_value";
    private static final String TABLE_COLUMN_HISTORY_CREATED_BY_USER = "history_created_by_user";
    private static final String TABLE_COLUMN_HISTORY_CREATED_AT = "history_created_at";
    private static final String TABLE_COLUMN_HISTORY_DATE = "history_date";
    private static final String TABLE_COLUMN_HISTORY_BALANCE_VALUE_BEFORE = "history_balance_value_before";
    private static final String TABLE_COLUMN_HISTORY_BALANCE_VALUE_AFTER = "history_balance_value_after";
    private static final String TABLE_COLUMN_HISTORY_KEY = "history_key";
    private static final String TABLE_COLUMN_HISTORY_UPDATED_AT = "history_updated_at";
    private static final String TABLE_COLUMN_HISTORY_UPDATED_BY_USER = "history_updated_by_user";
    private static final String TABLE_COLUMN_HISTORY_PREVIOUS_ITEM_KEY = "history_previous_item_key";
    private static final String TABLE_COLUMN_HISTORY_NEXT_ITEM_KEY = "history_next_item_key";

    private static final String CREATE_TABLE_BALANCES_QUERY = "CREATE TABLE " + TABLE_NAME_BALANCES
            + " (" + TABLE_COLUMN_BALANCE_ID + " INTEGER PRIMARY KEY, "
            + TABLE_COLUMN_BALANCE_NAME + " TEXT, "
            + TABLE_COLUMN_BALANCE_VALUE + " REAL, "
            + TABLE_COLUMN_BALANCE_START_VALUE + " REAL, "
            + TABLE_COLUMN_BALANCE_CREATED_AT + " INTEGER, "
            + TABLE_COLUMN_BALANCE_CREATED_BY_USER + " TEXT, "
            + TABLE_COLUMN_BALANCE_UPDATED_BY_USER + " TEXT, "
            + TABLE_COLUMN_BALANCE_CURRENCY_SYMBOL + " TEXT, "
            + TABLE_COLUMN_BALANCE_DESCRIPTION + " TEXT, "
            + TABLE_COLUMN_BALANCE_LAST_UPDATED_AT + " INTEGER, "
            + TABLE_COLUMN_BALANCE_IS_SYNCHRONIZABLE + " INTEGER, "
            + TABLE_COLUMN_BALANCE_KEY + " INTEGER, "
            + TABLE_COLUMN_BALANCE_EARNINGS + " REAL, "
            + TABLE_COLUMN_BALANCE_COSTS + " REAL, "
            + TABLE_COLUMN_BALANCE_PLANNED_EARNINGS + " REAL, "
            + TABLE_COLUMN_BALANCE_PLANNED_COSTS + " REAL"
            + ");";
    private static final String CREATE_TABLE_HISTORY_QUERY = "CREATE TABLE " + TABLE_NAME_HISTORY
            + " (" + TABLE_COLUMN_HISTORY_ID + " INTEGER PRIMARY KEY, "
            + TABLE_COLUMN_HISTORY_TYPE_ID + " INTEGER, "
            + TABLE_COLUMN_HISTORY_VALUE + " REAL, "
            + TABLE_COLUMN_HISTORY_DESCRIPTION + " TEXT, "
            + TABLE_COLUMN_HISTORY_CREATED_BY_USER + " TEXT, "
            + TABLE_COLUMN_HISTORY_UPDATED_BY_USER + " TEXT,"
            + TABLE_COLUMN_HISTORY_CREATED_AT + " INTEGER, "
            + TABLE_COLUMN_HISTORY_UPDATED_AT + " INTEGER, "
            + TABLE_COLUMN_HISTORY_KEY + " INTEGER, "
            + TABLE_COLUMN_HISTORY_PREVIOUS_ITEM_KEY + " INTEGER, "
            + TABLE_COLUMN_HISTORY_NEXT_ITEM_KEY + " INTEGER, "
            + TABLE_COLUMN_HISTORY_DATE + " INTEGER, "
            + TABLE_COLUMN_HISTORY_BALANCE_VALUE_BEFORE + " REAL, "
            + TABLE_COLUMN_HISTORY_BALANCE_VALUE_AFTER + " REAL, "
            + TABLE_COLUMN_HISTORY_BALANCE_ID + " INTEGER, FOREIGN KEY(" + TABLE_COLUMN_HISTORY_BALANCE_ID + ") REFERENCES " + TABLE_NAME_BALANCES + "(" + TABLE_COLUMN_BALANCE_ID + ") ON DELETE CASCADE"
            + ");";

    private static volatile DBHelper instance;

    public static DBHelper getInstance(Context context){
        DBHelper localInstance = instance;
        if (localInstance == null){
            synchronized (DBHelper.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new DBHelper(context);
                }
            }
        }
        return localInstance;
    }

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_BALANCES_QUERY);
        db.execSQL(CREATE_TABLE_HISTORY_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_BALANCES);//TODO maybe will be better not destroy table but update it
        db.execSQL(CREATE_TABLE_BALANCES_QUERY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_HISTORY);
        db.execSQL(CREATE_TABLE_HISTORY_QUERY);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys = ON;");
        }
    }

    private long addBalanceItem(BalanceItem balanceItem){
        return addBalanceItem(balanceItem, null, null);
    }

    private long addBalanceItem(BalanceItem balanceItem,@Nullable SQLiteDatabase database,@Nullable ContentValues contentValues){
        if(database == null)
            database = getWritableDatabase();
        if(contentValues == null)
            contentValues = new ContentValues();
        else
            contentValues.clear();
        contentValues = balanceItemToContentValues(balanceItem, contentValues);
        return database.insert(TABLE_NAME_BALANCES, null, contentValues);
    }

    private long addBalanceItems(List<BalanceItem> balanceItems){
        long added = 0;
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for(BalanceItem balanceItem : balanceItems){
            added += addBalanceItem(balanceItem, database, contentValues);
        }
        return added;
    }

    private int updateBalanceItem(BalanceItem balanceItem){
        return updateBalanceItem(balanceItem, null, null);
    }

    private int updateBalanceItem(BalanceItem balanceItem,@Nullable SQLiteDatabase database,@Nullable ContentValues contentValues){
        if(database == null)
            database = getWritableDatabase();
        if(contentValues == null)
            contentValues = new ContentValues();
        else
            contentValues.clear();
        contentValues = balanceItemToContentValues(balanceItem, contentValues);
        return database.update(TABLE_NAME_BALANCES, contentValues, TABLE_COLUMN_BALANCE_ID + " = ?", new String[]{String.valueOf(balanceItem.getId())});
    }

    private int updateBalanceItems(List<BalanceItem> balanceItems){
        int updated = 0;
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for(BalanceItem balanceItem : balanceItems){
            updated += updateBalanceItem(balanceItem, database, contentValues);
        }
        return updated;
    }

    private int deleteBalanceItem(BalanceItem balanceItem){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_NAME_BALANCES, TABLE_COLUMN_BALANCE_ID + " = " + balanceItem.getId(), null);
    }

    private int deleteBalanceItems(List<BalanceItem> balanceItems){
        SQLiteDatabase database = getWritableDatabase();
        StringBuilder stringBuilder = new StringBuilder(TABLE_COLUMN_BALANCE_ID + " IN (");
        for (int i = 0; i < balanceItems.size(); i++){
            BalanceItem balanceItem = balanceItems.get(i);
            stringBuilder.append(balanceItem.getId());
            if(i == balanceItems.size() - 1){
                stringBuilder.append(")");
            }else{
                stringBuilder.append(", ");
            }
        }
        return database.delete(TABLE_NAME_BALANCES, stringBuilder.toString(), null);
    }

    private int deleteAllBalances(){
        SQLiteDatabase database = getWritableDatabase();
//        db.execSQL("delete from "+ TABLE_NAME_BALANCES);
        return database.delete(TABLE_NAME_BALANCES, null, null);
    }

    private List<BalanceItem> getBalanceById(int balanceId){
        List<BalanceItem> balanceItems = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(true, TABLE_NAME_BALANCES, null, TABLE_COLUMN_BALANCE_ID + " = ?", new String[]{String.valueOf(balanceId)}, null, null, null, null);
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    do{
                        BalanceItem balanceItem = fromCursorToBalanceItem(cursor);
                        balanceItems.add(balanceItem);
                    }while(cursor.moveToNext());
                }
            }
            cursor.close();
        }
        return balanceItems;
    }

    private List<BalanceItem> getAllBalances(){
        List<BalanceItem> balanceItems = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(true, TABLE_NAME_BALANCES, null, null, null, null, null, null, null);
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    do{
                        BalanceItem balanceItem = fromCursorToBalanceItem(cursor);
                        balanceItems.add(balanceItem);
                    }while(cursor.moveToNext());
                }
            }
            cursor.close();
        }
        return balanceItems;
    }

    public int isBalanceNameAvailable(String balanceName){
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor = database.query(true, TABLE_NAME_BALANCES, null, TABLE_COLUMN_BALANCE_NAME + " = ?", new String[]{balanceName}, null, null, null, null);
        int id = Utils.NO_VALUE;
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    BalanceItem balanceItem = fromCursorToBalanceItem(cursor);
                    id = balanceItem.getId();
                }
            }
        }
        return id;
    }

    private long addHistoryItem(HistoryItem historyItem){
        return addHistoryItem(historyItem, null, null);
    }

    private long addHistoryItem(HistoryItem historyItem,@Nullable SQLiteDatabase database,@Nullable ContentValues contentValues){
        if(database == null)
            database = getWritableDatabase();
        if(contentValues == null)
            contentValues = new ContentValues();
        else
            contentValues.clear();
        contentValues = historyItemToContentValues(historyItem, contentValues);
        return database.insert(TABLE_NAME_HISTORY, null, contentValues);
    }

    private long addHistoryItems(List<HistoryItem> historyItems){
        long addedItems = 0;
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for(HistoryItem historyItem : historyItems){
            addedItems += addHistoryItem(historyItem, database, contentValues);
        }
        return addedItems;
    }

    private int updateHistoryItem(HistoryItem historyItem){
        return updateHistoryItem(historyItem, null, null);
    }

    private int updateHistoryItem(HistoryItem historyItem,@Nullable SQLiteDatabase database,@Nullable ContentValues contentValues){
        if(database == null)
            database = getWritableDatabase();
        if(contentValues == null)
            contentValues = new ContentValues();
        else
            contentValues.clear();
        contentValues = historyItemToContentValues(historyItem, contentValues);
        return database.update(TABLE_NAME_HISTORY, contentValues, TABLE_COLUMN_HISTORY_ID + " = ?", new String[]{String.valueOf(historyItem.getId())});
    }

    private int updateHistoryItems(List<HistoryItem> historyItems){
        int updated = 0;
        SQLiteDatabase database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        for(HistoryItem historyItem : historyItems){
            updated += updateHistoryItem(historyItem, database, contentValues);
        }
        return updated;
    }

    private int deleteHistoryItem(HistoryItem historyItem){
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_NAME_HISTORY, TABLE_COLUMN_HISTORY_ID + " = " + historyItem.getId(), null);
    }

    private int deleteHistoryItems(List<HistoryItem> historyItems){
        SQLiteDatabase database = getWritableDatabase();
        StringBuilder stringBuilder = new StringBuilder(TABLE_COLUMN_HISTORY_ID + " IN (");
        for (int i = 0; i < historyItems.size(); i++){
            HistoryItem historyItem = historyItems.get(i);
            stringBuilder.append(historyItem.getId());
            if(i == historyItems.size() - 1){
                stringBuilder.append(")");
            }else{
                stringBuilder.append(", ");
            }
        }
        return database.delete(TABLE_NAME_HISTORY, stringBuilder.toString(), null);
    }

    private List<HistoryItem> getHistoryItems(EDashboardPages dashboardPage, BalanceItem balanceItem){
        List<HistoryItem> historyItems = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor;
        if(dashboardPage == EDashboardPages.ALL){
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_BALANCE_ID + " = ?", new String[]{String.valueOf(balanceItem.getId())}, null, null, null, null);
        }else{
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_TYPE_ID + " = ? AND " + TABLE_COLUMN_HISTORY_BALANCE_ID + " = ?", new String[]{String.valueOf(dashboardPage.getId()), String.valueOf(balanceItem.getId())}, null, null, null, null);
        }
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    do{
                        historyItems.add(fromCursorToHistoryItem(cursor));
                    }while(cursor.moveToNext());
                }
            }
            cursor.close();
        }
        return historyItems;
    }

    private HashMap<Long, List<HistoryItem>> getHistoryItemsMap(EDashboardPages dashboardPage, BalanceItem balanceItem){
        HashMap<Long, List<HistoryItem>> historyItems = new HashMap<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor;
        if(dashboardPage == EDashboardPages.ALL){
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_BALANCE_ID + " = ?", new String[]{String.valueOf(balanceItem.getId())}, null, null, null, null);
        }else{
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_TYPE_ID + " = ? AND " + TABLE_COLUMN_HISTORY_BALANCE_ID + " = ?", new String[]{String.valueOf(dashboardPage.getId()), String.valueOf(balanceItem.getId())}, null, null, null, null);
        }
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    do{
                        HistoryItem item = fromCursorToHistoryItem(cursor);
                        if(historyItems.containsKey(item.getDate())){
                            historyItems.get(item.getDate()).add(item);
                        }else{
                            List<HistoryItem> items = new ArrayList<>();
                            items.add(item);
                            historyItems.put(item.getDate(), items);
                        }
                    }while(cursor.moveToNext());
                }
            }
            cursor.close();
        }
        return historyItems;
    }

    public List<HistoryItem> getHistoryItems(long startTime, EDashboardPages dashboardPage, BalanceItem balanceItem){
        List<HistoryItem> historyItems = new ArrayList<>();
        SQLiteDatabase database = getReadableDatabase();
        Cursor cursor;
        if(dashboardPage == EDashboardPages.ALL){
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_BALANCE_ID + " = ? AND " + TABLE_COLUMN_HISTORY_DATE + " <= ?", new String[]{String.valueOf(balanceItem.getId()), String.valueOf(startTime)}, null, null, null, String.valueOf(Constants.Common.HISTORY_ITEMS_PER_PAGE));
        }else {
            cursor = database.query(true, TABLE_NAME_HISTORY, null, TABLE_COLUMN_HISTORY_TYPE_ID + " = ? AND " + TABLE_COLUMN_HISTORY_BALANCE_ID + " = ? AND " + TABLE_COLUMN_HISTORY_DATE + " <= ?", new String[]{String.valueOf(dashboardPage.getId()), String.valueOf(balanceItem.getId()), String.valueOf(startTime)}, null, null, null, String.valueOf(Constants.Common.HISTORY_ITEMS_PER_PAGE));
        }
        if(cursor != null){
            if(cursor.getCount() > 0){
                if(cursor.moveToFirst()){
                    do{
                        historyItems.add(fromCursorToHistoryItem(cursor));
                    }while(cursor.moveToNext());
                }
            }
            cursor.close();
        }
        return historyItems;
    }

    private int deleteAllHistory(){
        SQLiteDatabase database = getWritableDatabase();
//        db.execSQL("delete from "+ TABLE_NAME_HISTORY);
        return database.delete(TABLE_NAME_HISTORY, null, null);
    }

    private ContentValues balanceItemToContentValues(BalanceItem balanceItem, ContentValues contentValues){
        contentValues.put(TABLE_COLUMN_BALANCE_NAME, balanceItem.getName());
        contentValues.put(TABLE_COLUMN_BALANCE_VALUE, balanceItem.getValue());
        contentValues.put(TABLE_COLUMN_BALANCE_START_VALUE, balanceItem.getStartValue());
        contentValues.put(TABLE_COLUMN_BALANCE_CREATED_AT, balanceItem.getCreatedAt());
        contentValues.put(TABLE_COLUMN_BALANCE_CREATED_BY_USER, balanceItem.getCreatedByUser());
        contentValues.put(TABLE_COLUMN_BALANCE_UPDATED_BY_USER, balanceItem.getUpdatedByUser());
        contentValues.put(TABLE_COLUMN_BALANCE_CURRENCY_SYMBOL, balanceItem.getCurrencySymbol());
        contentValues.put(TABLE_COLUMN_BALANCE_DESCRIPTION, balanceItem.getDescription());
        contentValues.put(TABLE_COLUMN_BALANCE_LAST_UPDATED_AT, balanceItem.getUpdatedAt());
        contentValues.put(TABLE_COLUMN_BALANCE_KEY, balanceItem.getKey());
        contentValues.put(TABLE_COLUMN_BALANCE_IS_SYNCHRONIZABLE, balanceItem.isSynchronizable());
        contentValues.put(TABLE_COLUMN_BALANCE_EARNINGS, balanceItem.getEarnings());
        contentValues.put(TABLE_COLUMN_BALANCE_COSTS, balanceItem.getCosts());
        contentValues.put(TABLE_COLUMN_BALANCE_PLANNED_EARNINGS, balanceItem.getPlannedEarnings());
        contentValues.put(TABLE_COLUMN_BALANCE_PLANNED_COSTS, balanceItem.getPlannedCosts());
        return contentValues;
    }

    private ContentValues historyItemToContentValues(HistoryItem historyItem, ContentValues contentValues){
        contentValues.put(TABLE_COLUMN_HISTORY_TYPE_ID, historyItem.getTypeId());
        contentValues.put(TABLE_COLUMN_HISTORY_BALANCE_ID, historyItem.getBalanceId());
        contentValues.put(TABLE_COLUMN_HISTORY_VALUE, historyItem.getValue());
        contentValues.put(TABLE_COLUMN_HISTORY_DESCRIPTION, historyItem.getDescription());
        contentValues.put(TABLE_COLUMN_HISTORY_CREATED_BY_USER, historyItem.getCreatedByUser());
        contentValues.put(TABLE_COLUMN_HISTORY_UPDATED_BY_USER, historyItem.getUpdatedByUser());
        contentValues.put(TABLE_COLUMN_HISTORY_CREATED_AT, historyItem.getCreatedAt());
        contentValues.put(TABLE_COLUMN_HISTORY_UPDATED_AT, historyItem.getUpdatedAt());
        contentValues.put(TABLE_COLUMN_HISTORY_KEY, historyItem.getKey());
        contentValues.put(TABLE_COLUMN_HISTORY_PREVIOUS_ITEM_KEY, historyItem.getConnectedItemKeyPrev());
        contentValues.put(TABLE_COLUMN_HISTORY_NEXT_ITEM_KEY, historyItem.getConnectedItemKeyNext());
        contentValues.put(TABLE_COLUMN_HISTORY_DATE, historyItem.getDate());
        contentValues.put(TABLE_COLUMN_HISTORY_BALANCE_VALUE_BEFORE, historyItem.getBalanceValueBefore());
        contentValues.put(TABLE_COLUMN_HISTORY_BALANCE_VALUE_AFTER, historyItem.getBalanceValueAfter());
        return contentValues;
    }

    public void asyncAddBalanceItem(final BalanceItem balanceItem, @Nullable final IDBWriteListener writeListener){
        if(balanceItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return addBalanceItem(balanceItem);
            }
        }.execute();
    }

    public void asyncAddBalanceItems(final List<BalanceItem> balanceItems,@Nullable final IDBWriteListener writeListener){
        if(balanceItems == null || balanceItems.isEmpty()){
            return;
        }
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return addBalanceItems(balanceItems);
            }
        }.execute();
    }

    public void asyncUpdateBalanceItem(final BalanceItem balanceItem,@Nullable final IDBWriteListener writeListener){
        if(balanceItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)updateBalanceItem(balanceItem);
            }
        }.execute();
    }

    public void asyncUpdateBalanceItems(final List<BalanceItem> balanceItems,@Nullable final IDBWriteListener writeListener){
        if(balanceItems == null || balanceItems.isEmpty()){
            return;
        }
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)updateBalanceItems(balanceItems);
            }
        }.execute();
    }

    public void asyncDeleteBalanceItem(final BalanceItem balanceItem,@Nullable final IDBWriteListener writeListener){
        if(balanceItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteBalanceItem(balanceItem);
            }
        }.execute();
    }

    public void asyncDeleteBalanceItems(final List<BalanceItem> balanceItems,@Nullable final IDBWriteListener writeListener){
        if(balanceItems == null || balanceItems.isEmpty())
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteBalanceItems(balanceItems);
            }
        }.execute();
    }

    public void asyncDeleteAllBalances(@Nullable final IDBWriteListener writeListener){
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteAllBalances();
            }
        }.execute();
    }

    public void asyncGetBalance(int balanceId, @Nullable final IDBReadListener<BalanceItem> readListener){
        new AsyncTask<Integer, Void, List<BalanceItem>>(){

            @Override
            protected List<BalanceItem> doInBackground(Integer... params) {
                return getBalanceById(params[0]);
            }

            @Override
            protected void onPostExecute(List<BalanceItem> items) {
                super.onPostExecute(items);
                if(readListener != null)
                    readListener.onReadFinish(items);
            }
        }.execute(balanceId);
    }

    public void asyncGetAllBalances(@Nullable final IDBReadListener<BalanceItem> readListener){
        new AsyncTask<Void, Void, List<BalanceItem>>(){

            @Override
            protected List<BalanceItem> doInBackground(Void... params) {
                return getAllBalances();
            }

            @Override
            protected void onPostExecute(List<BalanceItem> items) {
                super.onPostExecute(items);
                if(readListener != null)
                    readListener.onReadFinish(items);
            }
        }.execute();
    }

    public void asyncAddHistoryItem(final HistoryItem historyItem, @Nullable final IDBWriteListener writeListener){
        if(historyItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return addHistoryItem(historyItem);
            }
        }.execute();
    }

    public void asyncAddHistoryItems(final List<HistoryItem> historyItems,@Nullable final IDBWriteListener writeListener){
        if(historyItems == null || historyItems.isEmpty()){
            return;
        }
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return addHistoryItems(historyItems);
            }
        }.execute();
    }

    public void asyncUpdateHistoryItem(final HistoryItem historyItem,@Nullable final IDBWriteListener writeListener){
        if(historyItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)updateHistoryItem(historyItem);
            }
        }.execute();
    }

    public void asyncUpdateHistoryItems(final List<HistoryItem> historyItems,@Nullable final IDBWriteListener writeListener){
        if(historyItems == null || historyItems.isEmpty()){
            return;
        }
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)updateHistoryItems(historyItems);
            }
        }.execute();
    }

    public void asyncDeleteHistoryItem(final HistoryItem historyItem,@Nullable final IDBWriteListener writeListener){
        if(historyItem == null)
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteHistoryItem(historyItem);
            }
        }.execute();
    }

    public void asyncDeleteHistoryItems(final List<HistoryItem> historyItems,@Nullable final IDBWriteListener writeListener){
        if(historyItems == null || historyItems.isEmpty())
            return;
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteHistoryItems(historyItems);
            }
        }.execute();
    }

    public void asyncDeleteAllHistory(@Nullable final IDBWriteListener writeListener){
        new AsyncWriter(writeListener){
            @Override
            protected Long doInBackground(Void... params) {
                return (long)deleteAllHistory();
            }
        }.execute();
    }

    public void asyncGetHistory(final EDashboardPages dashboardPage, final BalanceItem balanceItem, @NonNull final IDBReadListener<HistoryItem> readListener){
        new AsyncTask<Void, Void, List<HistoryItem>>(){

            @Override
            protected List<HistoryItem> doInBackground(Void... params) {
                return getHistoryItems(dashboardPage, balanceItem);
            }

            @Override
            protected void onPostExecute(List<HistoryItem> items) {
                super.onPostExecute(items);
                readListener.onReadFinish(items);
            }
        }.execute();
    }

    public void asyncGetHistoryMap(final EDashboardPages dashboardPage, final BalanceItem balanceItem, @NonNull final IDBReadListener<List<HistoryItem>> readListener){
        new AsyncTask<Void, Void, TreeMap<Long, List<HistoryItem>>>(){

            @Override
            protected TreeMap<Long, List<HistoryItem>> doInBackground(Void... params) {
                return new TreeMap<>(getHistoryItemsMap(dashboardPage, balanceItem));
            }

            @Override
            protected void onPostExecute(TreeMap<Long, List<HistoryItem>> items) {
                super.onPostExecute(items);
                readListener.onReadFinish(items);
            }
        }.execute();
    }

    public void asyncGetHistory(final EDashboardPages dashboardPage, final BalanceItem balanceItem, final long startTime, @NonNull final IDBReadListener<HistoryItem> readListener){
        new AsyncTask<Void, Void, List<HistoryItem>>(){

            @Override
            protected List<HistoryItem> doInBackground(Void... params) {
                return getHistoryItems(startTime, dashboardPage, balanceItem);
            }

            @Override
            protected void onPostExecute(List<HistoryItem> items) {
                super.onPostExecute(items);
                readListener.onReadFinish(items);
            }
        }.execute();
    }

    private BalanceItem fromCursorToBalanceItem(Cursor cursor){
        BalanceItem balanceItem = new BalanceItem();
        balanceItem.setId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_ID)));
        balanceItem.setSynchronizable(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_IS_SYNCHRONIZABLE)) > 0);
        balanceItem.setCreatedAt(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_CREATED_AT)));
        balanceItem.setUpdatedAt(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_LAST_UPDATED_AT)));
        balanceItem.setValue(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_VALUE)));
        balanceItem.setStartValue(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_START_VALUE)));
        balanceItem.setEarnings(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_EARNINGS)));
        balanceItem.setCosts(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_COSTS)));
        balanceItem.setPlannedEarnings(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_PLANNED_EARNINGS)));
        balanceItem.setPlannedCosts(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_PLANNED_COSTS)));
        balanceItem.setName(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_NAME)));
        balanceItem.setCreatedByUser(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_CREATED_BY_USER)));
        balanceItem.setUpdatedByUser(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_UPDATED_BY_USER)));
        balanceItem.setCurrencySymbol(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_CURRENCY_SYMBOL)));
        balanceItem.setDescription(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_DESCRIPTION)));
        balanceItem.setKey(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_BALANCE_KEY)));
        return balanceItem;
    }

    private HistoryItem fromCursorToHistoryItem(Cursor cursor){
        HistoryItem historyItem = new HistoryItem();
        historyItem.setId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_ID)));
        historyItem.setTypeId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_TYPE_ID)));
        historyItem.setValue(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_VALUE)));
        historyItem.setDescription(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_DESCRIPTION)));
        historyItem.setCreatedByUser(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_CREATED_BY_USER)));
        historyItem.setUpdatedByUser(cursor.getString(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_UPDATED_BY_USER)));
        historyItem.setCreatedAt(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_CREATED_AT)));
        historyItem.setBalanceId(cursor.getInt(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_BALANCE_ID)));
        historyItem.setDate(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_DATE)));
        historyItem.setBalanceValueBefore(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_BALANCE_VALUE_BEFORE)));
        historyItem.setBalanceValueAfter(cursor.getDouble(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_BALANCE_VALUE_AFTER)));
        historyItem.setUpdatedAt(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_UPDATED_AT)));
        historyItem.setKey(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_KEY)));
        historyItem.setConnectedItemKeyPrev(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_PREVIOUS_ITEM_KEY)));
        historyItem.setConnectedItemKeyNext(cursor.getLong(cursor.getColumnIndex(TABLE_COLUMN_HISTORY_NEXT_ITEM_KEY)));
        return historyItem;
    }
}
