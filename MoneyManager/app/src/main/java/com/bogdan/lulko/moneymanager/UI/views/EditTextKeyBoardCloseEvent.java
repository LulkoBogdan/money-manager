package com.bogdan.lulko.moneymanager.UI.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IKeyBoardCloseListener;

public class EditTextKeyBoardCloseEvent extends EditText{

    private IKeyBoardCloseListener keyBoardCloseListener;

    public EditTextKeyBoardCloseEvent(Context context) {
        super(context);
    }

    public EditTextKeyBoardCloseEvent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextKeyBoardCloseEvent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        Utils.log("QWERTY event.getKeyCode() = " + event.getKeyCode() + "   KeyEvent.KEYCODE_BACK = " + KeyEvent.KEYCODE_BACK + "   KeyEvent.KEYCODE_ENTER = " + KeyEvent.KEYCODE_ENTER);
        if ((event.getKeyCode() == KeyEvent.KEYCODE_BACK || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && event.getAction() == KeyEvent.ACTION_UP) {
            if (keyBoardCloseListener != null)
                keyBoardCloseListener.onKeyBoardClose();
        }
        return super.dispatchKeyEvent(event);
    }

    public void setKeyBoardCloseListener(IKeyBoardCloseListener keyBoardCloseListener){
        this.keyBoardCloseListener = keyBoardCloseListener;
    }

}
