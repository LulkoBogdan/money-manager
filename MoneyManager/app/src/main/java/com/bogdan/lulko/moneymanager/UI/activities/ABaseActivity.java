package com.bogdan.lulko.moneymanager.UI.activities;

import android.app.Activity;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.UI.fragments.DashboardFragment;
import com.bogdan.lulko.moneymanager.UI.fragments.LoginFragment;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Utils;

public abstract class ABaseActivity extends AppCompatActivity{

    public void changeFragment(Fragment fragment, boolean popToBackStack, boolean clearBackStack, boolean withAnimation){
        hideKeyBoard(getWindow().getDecorView().getWindowToken());
        if (clearBackStack) {
            if(Utils.getBooleanFromPrefs(Constants.PrefKeys.KEY_IS_LOGGED_IN, false, this)) {
                getSupportFragmentManager().popBackStack(DashboardFragment.class.getName(), 0);
            }else{
                getSupportFragmentManager().popBackStack(LoginFragment.class.getName(), 0);
            }
        }
        if (fragment == null || getSupportFragmentManager().findFragmentByTag(fragment.getClass().getName()) != null){
            return;
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        if (withAnimation)
//            transaction.setCustomAnimations(enter, exit, popEnter, popExit);
        transaction.replace(R.id.container, fragment, fragment.getClass().getName());
        if (popToBackStack)
            transaction.addToBackStack(fragment.getClass().getName());
        transaction.commit();
    }

    public void hideKeyBoard(){
        hideKeyBoard(getWindow().getDecorView().getWindowToken());
    }

    public void hideKeyBoard(IBinder windowToken){
        InputMethodManager inputMethodManager = (InputMethodManager)  getSystemService(Activity.INPUT_METHOD_SERVICE);
        if(inputMethodManager != null){
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0);
        }
    }

}
