package com.bogdan.lulko.moneymanager.helpers;

import com.bogdan.lulko.moneymanager.models.CountryList;
import com.bogdan.lulko.moneymanager.models.CountryList.Country;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class JsonDecoder {

    public static CountryList decodeCountries(InputStream is) {
        CountryList result = new CountryList();
        String json = null;
        try{
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            JSONObject jsonObject = new JSONObject(json);
            result.countries = decodeCountriesList(jsonObject.getJSONArray("countries"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static List<Country> decodeCountriesList(JSONArray jsonArray) {
        List<Country> result = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++){
            try {
                JSONObject item = jsonArray.getJSONObject(i);
                result.add(new Country(item.getString("country"), item.getString("countryISO")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

}
