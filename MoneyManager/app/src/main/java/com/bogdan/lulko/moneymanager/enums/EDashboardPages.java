package com.bogdan.lulko.moneymanager.enums;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import com.bogdan.lulko.moneymanager.R;

public enum EDashboardPages {

    ALL(R.string.page_all, 0, R.string.page_empty_view_text_all, R.color.textColorPrimary),
    EARNINGS(R.string.page_earnings, 1, R.string.page_empty_view_text_earnings, R.color.text_color_earnings),
    COSTS(R.string.page_costs, 2, R.string.page_empty_view_text_costs, R.color.text_color_costs),
    PLANNED_EARNINGS(R.string.page_planned_earnings, 3, R.string.page_empty_view_text_planned_earnings, R.color.text_color_planned_earnings),
    PLANNED_COSTS(R.string.page_planned_costs, 4, R.string.page_empty_view_text_planned_costs, R.color.text_color_planned_costs);

    private int nameResId;
    private int id;
    private int emptyViewTextId;
    private int colorId;

    EDashboardPages(int nameResId, int id, int emptyViewTextId, int colorId){
        this.nameResId = nameResId;
        this.id = id;
        this.emptyViewTextId = emptyViewTextId;
        this.colorId = colorId;
    }

    public String getName(Context context){
        return context.getString(nameResId);
    }

    public int getNameResId(){
        return nameResId;
    }

    public int getEmptyViewTextId(){
        return emptyViewTextId;
    }

    public String getEmptyViewText(Context context){
        return context.getString(emptyViewTextId);
    }

    public int getId(){
        return id;
    }

    public int getColorId(){
        return colorId;
    }

    public int getColor(Context context){
        return ContextCompat.getColor(context, colorId);
    }

    public static @NonNull EDashboardPages getPageById(int id){
        for(EDashboardPages dashboardPage : EDashboardPages.values()){
            if(dashboardPage.getId() == id){
                return dashboardPage;
            }
        }
        return ALL;
    }
}
