package com.bogdan.lulko.moneymanager.interfaces;

public interface ITwoButtonDialogCallback {
    void onLeftButtonClick();
    void onRightButtonClick();
    void onCancel();
}
