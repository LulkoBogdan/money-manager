package com.bogdan.lulko.moneymanager.models;

public class HistoryItem {

    private int id;
    private int balanceId;
    private int typeId;
    private long createdAt;
    private long date;
    private long key;
    private long connectedItemKeyPrev;
    private long connectedItemKeyNext;
    private long updatedAt;
    private double value;
    private double balanceValueBefore;
    private double balanceValueAfter;
    private String description;
    private String createdByUser;
    private String updatedByUser;

    public HistoryItem() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(int balanceId) {
        this.balanceId = balanceId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getUpdatedByUser() {
        return updatedByUser;
    }

    public void setUpdatedByUser(String updatedByUser) {
        this.updatedByUser = updatedByUser;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getBalanceValueBefore() {
        return balanceValueBefore;
    }

    public void setBalanceValueBefore(double balanceValueBefore) {
        this.balanceValueBefore = balanceValueBefore;
    }

    public double getBalanceValueAfter() {
        return balanceValueAfter;
    }

    public void setBalanceValueAfter(double balanceValueAfter) {
        this.balanceValueAfter = balanceValueAfter;
    }

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public long getConnectedItemKeyPrev() {
        return connectedItemKeyPrev;
    }

    public void setConnectedItemKeyPrev(long connectedItemKeyPrev) {
        this.connectedItemKeyPrev = connectedItemKeyPrev;
    }

    public long getConnectedItemKeyNext() {
        return connectedItemKeyNext;
    }

    public void setConnectedItemKeyNext(long connectedItemKeyNext) {
        this.connectedItemKeyNext = connectedItemKeyNext;
    }

    @Override
    public String toString() {
        return "HistoryItem ={id = " + id +
                ", balanceId = " + balanceId +
                ", typeId = " + typeId +
                ", createdAt = " + createdAt +
                ", date = " + date +
                ", key = " + key +
                ", connectedItemKeyPrev = " + connectedItemKeyPrev +
                ", connectedItemKeyNext = " + connectedItemKeyNext +
                ", updatedAt = " + updatedAt +
                ", value = " + value +
                ", balanceValueBefore = " + balanceValueBefore +
                ", balanceValueAfter = " + balanceValueAfter +
                ", createdByUser = " + createdByUser +
                ", updatedByUser = " + updatedByUser +
                ", description = " + description + "}";
    }
}
