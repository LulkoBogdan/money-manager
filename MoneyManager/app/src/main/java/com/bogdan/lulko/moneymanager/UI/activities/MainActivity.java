package com.bogdan.lulko.moneymanager.UI.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.UI.fragments.BalanceManagerFragment;
import com.bogdan.lulko.moneymanager.UI.fragments.DashboardFragment;
import com.bogdan.lulko.moneymanager.UI.fragments.LoginFragment;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.DBHelper;
import com.bogdan.lulko.moneymanager.helpers.Dialogs;
import com.bogdan.lulko.moneymanager.helpers.FormatterHelper;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.interfaces.ITwoButtonDialogCallback;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

import java.util.List;
import java.util.Map;

public class MainActivity extends ABaseActivity {
//TODO all dimensions in layouts move to resources
//TODO check all layouts and its animations on API => 21
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView tvBalanceName;
    private TextView tvBalanceValue;
    private TextView tvUserName;

    private DBHelper dbHelper;
    private BalanceItem selectedBalance;

    private boolean firstClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        tvBalanceName = (TextView) navigationView.findViewById(R.id.textViewBalanceName);
        tvBalanceValue = (TextView) navigationView.findViewById(R.id.textViewBalanceValue);
        tvUserName = (TextView) navigationView.findViewById(R.id.textViewUserName);
        navigationView.setItemBackgroundResource(R.drawable.navigation_drawer_item_background);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                boolean close = true;
                switch (menuItem.getItemId()) {
                    case R.id.action_drawer_logout:
                        close = false;
                        Dialogs.showCustomTwoButtonDialog(MainActivity.this, R.string.btn_label_yes, R.string.btn_label_cancel, R.string.dialog_message_log_out, dialogCallback, true);
                        break;
                    case R.id.action_drawer_settings:
                        //TODO
                        break;
                    case R.id.action_drawer_balance_manager:
                        changeFragment(BalanceManagerFragment.getInstance(false), true, false, true);
                        break;
                    case R.id.action_drawer_home:
                        getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        break;
//                    case R.id.action_dashboard_chart:
//                        changeFragment(ChartFragment.getInstance(currentPage), true, false, true);
//                        break;
                }
//                menuItem.setChecked(true);
                if (close)
                    drawerLayout.closeDrawers();
                return true;
            }
        });
        dbHelper = DBHelper.getInstance(this);
        if(Utils.getBooleanFromPrefs(Constants.PrefKeys.KEY_IS_LOGGED_IN, false, this)){
            changeFragment(new DashboardFragment(), false, true, false);
        }else {
            changeFragment(new LoginFragment(), false, true, false);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }

    public DBHelper getDbHelper(){
        if(dbHelper == null){
            dbHelper = DBHelper.getInstance(this);
        }
        return dbHelper;
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(GravityCompat.START)){
            drawerLayout.closeDrawers();
            return;
        }
        if(getSupportFragmentManager().getBackStackEntryCount() > 0) {
            super.onBackPressed();
        }else if (!firstClick){
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    firstClick = false;
                }
            }, 2000);
            firstClick = true;
            Toast.makeText(this, R.string.message_press_back_more, Toast.LENGTH_SHORT).show();
        } else {
            hideKeyBoard(getWindow().getDecorView().getWindowToken());
            getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//            finish();
            super.onBackPressed();
        }
    }

    public BalanceItem getSelectedBalanceItem(){
        return selectedBalance;
    }

    public void setSelectedBalance(BalanceItem selectedBalance){
        this.selectedBalance = selectedBalance;
        updateNavigationDrawer();
        if(selectedBalance != null) {
            Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, selectedBalance.getId(), MainActivity.this);
        }else{
            Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, Utils.NO_VALUE, MainActivity.this);
        }
    }

    public void setSelectedBalance(int id){
        dbHelper.asyncGetBalance(id, new IDBReadListener<BalanceItem>() {
            @Override
            public void onReadFinish(List<BalanceItem> items) {
                if(items == null || items.isEmpty()) {
                    selectedBalance = null;
                    Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, Utils.NO_VALUE, MainActivity.this);
                }else {
                    selectedBalance = items.get(0);
                    Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, selectedBalance.getId(), MainActivity.this);
                }
                updateNavigationDrawer();
            }
            @Override
            public void onReadFinish(Map<Long, BalanceItem> items) {}
        });
    }

    public void openDrawer(){
        drawerLayout.openDrawer(GravityCompat.START);
    }

    public void closeDrawer(){
        drawerLayout.closeDrawers();
    }

    private ITwoButtonDialogCallback dialogCallback = new ITwoButtonDialogCallback() {
        @Override
        public void onLeftButtonClick() {
            Utils.putBooleanToPrefs(Constants.PrefKeys.KEY_IS_LOGGED_IN, false, MainActivity.this);
            changeFragment(new LoginFragment(), false, true, true);
        }
        @Override
        public void onRightButtonClick() {}
        @Override
        public void onCancel() {}
    };

    private void updateNavigationDrawer(){
        tvUserName.setText("Local");//TODO
        if(selectedBalance == null){
            tvBalanceName.setText("");
            tvBalanceValue.setText("");
        }else{
            tvBalanceName.setText(selectedBalance.getName());
            tvBalanceValue.setText(FormatterHelper.formatBalanceValueString(selectedBalance));
            if(selectedBalance.getValue() > 0){
                tvBalanceValue.setTextColor(ContextCompat.getColor(this, R.color.text_color_earnings));
            }else if(selectedBalance.getValue() < 0){
                tvBalanceValue.setTextColor(ContextCompat.getColor(this, R.color.text_color_costs));
            }else{
                tvBalanceValue.setTextColor(ContextCompat.getColor(this, R.color.textColorPrimary));
            }
        }
    }

    public void setSelectedDrawerNavigationItem(int position){
        navigationView.getMenu().getItem(position).setChecked(true);
    }

    @Override
    public void changeFragment(Fragment fragment, boolean popToBackStack, boolean clearBackStack, boolean withAnimation) {
        drawerLayout.closeDrawers();
        super.changeFragment(fragment, popToBackStack, clearBackStack, withAnimation);
    }
}
