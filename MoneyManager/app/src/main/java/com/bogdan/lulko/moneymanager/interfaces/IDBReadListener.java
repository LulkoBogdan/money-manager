package com.bogdan.lulko.moneymanager.interfaces;

import java.util.Map;
import java.util.List;

public interface IDBReadListener<T> {
    void onReadFinish(List<T> items);
    void onReadFinish(Map<Long, T> items);
}
