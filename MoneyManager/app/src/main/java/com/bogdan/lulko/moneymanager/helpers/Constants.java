package com.bogdan.lulko.moneymanager.helpers;

public class Constants {

    public static class Common{
        public static final String SHARED_PREFERENCES_NAME = "money_manager_prefs";

        public static final int MAX_BALANCE_COUNT = 10;
        public static final int HISTORY_ITEMS_PER_PAGE = 30;
    }

    public static class BundleKeys{
        public static final String KEY_PAGE_TYPE_ID = "key_page_type_id";
        public static final String KEY_GO_TO_NEW = "key_go_to_new";
        public static final String KEY_BALANCE_ITEM_TO_EDIT = "key_balance_item_to_edit";
    }

    public static class PrefKeys {
        public static final String KEY_IS_LOGGED_IN = "is_logged_in";
        public static final String KEY_SELECTED_BALANCE = "key_selected_balance";
    }

    public static class NavigationDrawer{
        public static final int ITEM_HOME_POSITION = 0;
        public static final int ITEM_MANAGE_BALANCES_POSITION = 1;
        public static final int ITEM_SETTINGS_POSITION = 2;
    }

}
