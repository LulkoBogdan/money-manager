package com.bogdan.lulko.moneymanager.models;

import android.os.Parcel;
import android.os.Parcelable;

public class BalanceItem implements Parcelable{

    private boolean synchronizable;
    private int id;
    private long createdAt;
    private long updatedAt;
    private long key;
    private double value;
    private double startValue;
    private double earnings;
    private double costs;
    private double plannedEarnings;
    private double plannedCosts;
    private String name;
    private String createdByUser;
    private String currencySymbol;
    private String description;
    private String updatedByUser;

    public BalanceItem() {
    }

    protected BalanceItem(Parcel in) {
        synchronizable = in.readByte() != 0;
        id = in.readInt();
        createdAt = in.readLong();
        updatedAt = in.readLong();
        key = in.readLong();
        value = in.readDouble();
        startValue = in.readDouble();
        earnings = in.readDouble();
        costs = in.readDouble();
        plannedEarnings = in.readDouble();
        plannedCosts = in.readDouble();
        name = in.readString();
        createdByUser = in.readString();
        updatedByUser = in.readString();
        currencySymbol = in.readString();
        description = in.readString();
    }

    public static final Creator<BalanceItem> CREATOR = new Creator<BalanceItem>() {
        @Override
        public BalanceItem createFromParcel(Parcel in) {
            return new BalanceItem(in);
        }

        @Override
        public BalanceItem[] newArray(int size) {
            return new BalanceItem[size];
        }
    };

    public boolean isSynchronizable() {
        return synchronizable;
    }

    public void setSynchronizable(boolean synchronizable) {
        this.synchronizable = synchronizable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    public long getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getEarnings() {
        return earnings;
    }

    public void setEarnings(double earnings) {
        this.earnings = earnings;
    }

    public double getCosts() {
        return costs;
    }

    public void setCosts(double costs) {
        this.costs = costs;
    }

    public double getPlannedEarnings() {
        return plannedEarnings;
    }

    public void setPlannedEarnings(double plannedEarnings) {
        this.plannedEarnings = plannedEarnings;
    }

    public double getPlannedCosts() {
        return plannedCosts;
    }

    public void setPlannedCosts(double plannedCosts) {
        this.plannedCosts = plannedCosts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedByUser() {
        return createdByUser;
    }

    public void setCreatedByUser(String createdByUser) {
        this.createdByUser = createdByUser;
    }

    public String getUpdatedByUser() {
        return updatedByUser;
    }

    public void setUpdatedByUser(String updatedByUser) {
        this.updatedByUser = updatedByUser;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStartValue() {
        return startValue;
    }

    public void setStartValue(double startValue) {
        this.startValue = startValue;
    }

    public long getKey() {
        return key;
    }

    public void setKey(long key) {
        this.key = key;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (synchronizable ? 1 : 0));
        dest.writeInt(id);
        dest.writeLong(createdAt);
        dest.writeLong(updatedAt);
        dest.writeLong(key);
        dest.writeDouble(value);
        dest.writeDouble(startValue);
        dest.writeDouble(earnings);
        dest.writeDouble(costs);
        dest.writeDouble(plannedEarnings);
        dest.writeDouble(plannedCosts);
        dest.writeString(name);
        dest.writeString(createdByUser);
        dest.writeString(updatedByUser);
        dest.writeString(currencySymbol);
        dest.writeString(description);
    }

    @Override
    public String toString() {
        return "BalanceItem ={synchronizable = " + synchronizable +
                ", id = " + id +
                ", createdAt = " + createdAt +
                ", updatedAt = " + updatedAt +
                ", key = " + key +
                ", value = " + value +
                ", startValue = " + startValue +
                ", earnings = " + earnings +
                ", costs = " + costs +
                ", plannedEarnings = " + plannedEarnings +
                ", plannedCosts = " + plannedCosts +
                ", name = " + name +
                ", createdByUser = " + createdByUser +
                ", updatedByUser = " + updatedByUser +
                ", currencySymbol = " + currencySymbol+
                ", description = " + description + "}";
     }
}
