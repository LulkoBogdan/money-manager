package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.adapters.ViewPagerAdapter;
import com.bogdan.lulko.moneymanager.enums.EDashboardPages;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Dialogs;
import com.bogdan.lulko.moneymanager.helpers.FormatterHelper;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.interfaces.ITwoButtonDialogCallback;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashboardFragment extends ABaseFragment {

    private Toolbar toolbar;

    private EDashboardPages currentPage = EDashboardPages.ALL;
    private HashMap<Integer, Fragment> fragments = new HashMap<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        initView(rootView);
        setSelectedDrawerPosition(Constants.NavigationDrawer.ITEM_HOME_POSITION);
        return rootView;
    }

    private void initView(View rootView){
        toolbar = initToolbar(R.id.toolbar, 0, R.mipmap.ic_menu_black_36dp, rootView, true);
        checkBalance();
        final ViewPager viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        setupViewPager(viewPager);
        TabLayout tabLayout = (TabLayout)rootView.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                currentPage = EDashboardPages.getPageById(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        FloatingActionButton fabPlus = (FloatingActionButton) rootView.findViewById(R.id.fabPlus);
        fabPlus.setOnClickListener(onPlusClickListener);
    }

    private void setupViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        addHistoryFragment(EDashboardPages.ALL, adapter);
        addHistoryFragment(EDashboardPages.EARNINGS, adapter);
        addHistoryFragment(EDashboardPages.COSTS, adapter);
        addHistoryFragment(EDashboardPages.PLANNED_EARNINGS, adapter);
        addHistoryFragment(EDashboardPages.PLANNED_COSTS, adapter);
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void addHistoryFragment(EDashboardPages dashboardPage, ViewPagerAdapter adapter){
        Fragment fragment;
        if(!fragments.containsKey(dashboardPage.getId())) {
            fragment = HistoryFragment.getInstance(currentPage);
            fragments.put(dashboardPage.getId(), fragment);
        }else{
            fragment = fragments.get(dashboardPage.getId());
        }
        adapter.addFragment(fragment, dashboardPage.getName(getActivity()));
    }

    private View.OnClickListener onPlusClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeFragment(AddHistoryItemFragment.getInstance(currentPage), true, false, true);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dashboard, menu);
//        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_dashboard_balance_info:
                changeFragment(InfoBalanceFragment.getInstance(getSelectedBalanceItem()), true, false, true);
                break;
            case android.R.id.home:
                openDrawer();
                return true;
            case R.id.action_dashboard_update:
                //TODO
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private ITwoButtonDialogCallback noBalancesListener = new ITwoButtonDialogCallback() {
        @Override
        public void onLeftButtonClick() {
            changeFragment(BalanceManagerFragment.getInstance(true), true, false, true);
        }

        @Override
        public void onRightButtonClick() {
            getActivity().finish();
        }
        @Override
        public void onCancel() {}
    };

    private void checkBalance(){
        int balanceId = Utils.getIntFromPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, Utils.NO_VALUE, getActivity());
        if(balanceId == Utils.NO_VALUE){
            getDBHelper().asyncGetAllBalances(readListener);
        }else{
            getDBHelper().asyncGetBalance(balanceId, readListener);
        }
    }

    private IDBReadListener<BalanceItem> readListener = new IDBReadListener<BalanceItem>() {
        @Override
        public void onReadFinish(List<BalanceItem> items) {
            if (items != null && !items.isEmpty()) {
                BalanceItem selectedBalanceItem = items.get(0);
                setSelectedBalanceItem(selectedBalanceItem);
                Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, selectedBalanceItem.getId(), getActivity());
                toolbar.setTitle(selectedBalanceItem.getName());
                toolbar.setSubtitle(FormatterHelper.formatBalanceValueString(selectedBalanceItem));
                if(selectedBalanceItem.getValue() > 0){
                    toolbar.setSubtitleTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_earnings));
                }else if(selectedBalanceItem.getValue() < 0){
                    toolbar.setSubtitleTextColor(ContextCompat.getColor(getActivity(), R.color.text_color_costs));
                }else{
                    toolbar.setSubtitleTextColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));
                }
            } else {
                setSelectedBalanceItem(null);
                Utils.putIntToPrefs(Constants.PrefKeys.KEY_SELECTED_BALANCE, Utils.NO_VALUE, getActivity());
                Dialogs.showCustomTwoButtonDialog(getActivity(), R.string.btn_label_ok, R.string.btn_label_exit, R.string.dialog_message_no_balances, noBalancesListener, false);
            }
        }
        @Override
        public void onReadFinish(Map<Long, BalanceItem> items) {}
    };
}
