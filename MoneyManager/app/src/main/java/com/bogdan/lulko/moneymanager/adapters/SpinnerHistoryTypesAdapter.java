package com.bogdan.lulko.moneymanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.enums.EDashboardPages;

import java.util.List;

public class SpinnerHistoryTypesAdapter extends ArrayAdapter<EDashboardPages> {

    private int resource;
    private int resourceDropdown;

    public SpinnerHistoryTypesAdapter(Context context, int resource, List<EDashboardPages> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
            viewHolder.llDivider = (LinearLayout) convertView.findViewById(R.id.ll_divider);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        EDashboardPages item = getItem(position);
        viewHolder.tvTitle.setText(item.getName(getContext()));
        viewHolder.tvTitle.setTextColor(item.getColor(getContext()));
        viewHolder.llDivider.setVisibility(View.GONE);
        return convertView;
    }

    @Override
    public void setDropDownViewResource(int resource) {
        super.setDropDownViewResource(resource);
        this.resourceDropdown = resource;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(resourceDropdown, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.textViewTitle);
            viewHolder.llDivider = (LinearLayout) convertView.findViewById(R.id.ll_divider);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        EDashboardPages item = getItem(position);
        viewHolder.tvTitle.setText(item.getName(getContext()));
        viewHolder.tvTitle.setTextColor(item.getColor(getContext()));
        if(position == getCount() - 1)
            viewHolder.llDivider.setVisibility(View.GONE);
        else
            viewHolder.llDivider.setVisibility(View.VISIBLE);
        return convertView;
    }

    private class ViewHolder{
        TextView tvTitle;
        LinearLayout llDivider;
    }
}
