package com.bogdan.lulko.moneymanager.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.models.BalanceCurrency;

import java.util.List;

public class SpinnerCurrenciesAdapter extends ArrayAdapter<BalanceCurrency> {

    private int resource;
    private int resourceDropdown;

    public SpinnerCurrenciesAdapter(Context context, int resource, List<BalanceCurrency> objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    @Override
    public void setDropDownViewResource(int resource) {
        super.setDropDownViewResource(resource);
        this.resourceDropdown = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.tvCountryName = (TextView) convertView.findViewById(R.id.textViewCountryName);
            viewHolder.tvCurrencySymbol = (TextView) convertView.findViewById(R.id.textViewCurrencySymbol);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        BalanceCurrency item = getItem(position);
        viewHolder.tvCountryName.setText(item.getCountryName());
        viewHolder.tvCurrencySymbol.setText(item.getCurrencySymbol());
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolderDropdown viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolderDropdown();
            convertView = LayoutInflater.from(getContext()).inflate(resourceDropdown, parent, false);
            viewHolder.tvCountryName = (TextView) convertView.findViewById(R.id.textViewCountryName);
            viewHolder.tvCurrencySymbol = (TextView) convertView.findViewById(R.id.textViewCurrencySymbol);
            viewHolder.tvCurrencyName = (TextView) convertView.findViewById(R.id.textViewCurrencyName);
            viewHolder.llDivider = (LinearLayout) convertView.findViewById(R.id.ll_divider);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderDropdown) convertView.getTag();
        }
        BalanceCurrency item = getItem(position);
        viewHolder.tvCountryName.setText(item.getCountryName());
        if(position == getCount() - 1)
            viewHolder.llDivider.setVisibility(View.GONE);
        else
            viewHolder.llDivider.setVisibility(View.VISIBLE);
        if(TextUtils.isEmpty(item.getCurrencySymbol()) && TextUtils.isEmpty(item.getCurrencyName())){
            viewHolder.tvCurrencySymbol.setVisibility(View.GONE);
            viewHolder.tvCurrencyName.setVisibility(View.GONE);
        } else {
            viewHolder.tvCurrencySymbol.setVisibility(View.VISIBLE);
            viewHolder.tvCurrencyName.setVisibility(View.VISIBLE);
            viewHolder.tvCurrencySymbol.setText(item.getCurrencySymbol());
            viewHolder.tvCurrencyName.setText(item.getCurrencyName());
        }
        return convertView;
    }

    private class ViewHolder{
        TextView tvCountryName;
        TextView tvCurrencySymbol;
    }

    private class ViewHolderDropdown extends ViewHolder{
        TextView tvCurrencyName;
        LinearLayout llDivider;
    }
}
