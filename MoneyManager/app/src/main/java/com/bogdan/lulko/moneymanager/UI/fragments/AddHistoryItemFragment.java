package com.bogdan.lulko.moneymanager.UI.fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.adapters.SpinnerHistoryTypesAdapter;
import com.bogdan.lulko.moneymanager.enums.EButtonTypes;
import com.bogdan.lulko.moneymanager.enums.EDashboardPages;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Dialogs;
import com.bogdan.lulko.moneymanager.helpers.Utils;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.interfaces.IDBWriteListener;
import com.bogdan.lulko.moneymanager.interfaces.IOneButtonDialogCallback;
import com.bogdan.lulko.moneymanager.models.BalanceItem;
import com.bogdan.lulko.moneymanager.models.HistoryItem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class AddHistoryItemFragment extends ABaseFragment {

    private TextInputLayout tilValue;
    private EditText etValue;
    private EditText etDescription;
    private TextView tvDate;

    private EDashboardPages currentPage = EDashboardPages.ALL;
    private List<EDashboardPages> types;

    private long date;

    public static AddHistoryItemFragment getInstance(EDashboardPages currentPage){
        AddHistoryItemFragment fragment = new AddHistoryItemFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BundleKeys.KEY_PAGE_TYPE_ID, currentPage.getId());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        initPageType();
        View rootView = inflater.inflate(R.layout.fragment_add_item, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initPageType(){
        Bundle arguments = getArguments();
        if(arguments != null){
            currentPage = EDashboardPages.getPageById(arguments.getInt(Constants.BundleKeys.KEY_PAGE_TYPE_ID));
        }
    }

    private void initView(View rootView){
        initToolbar(R.id.app_bar, R.string.new_history_item_toolbar_title, R.mipmap.ic_arrow_back_black_36dp, rootView, true);
        Button btnSave = (Button) rootView.findViewById(R.id.buttonSave);
        Button btnCancel = (Button) rootView.findViewById(R.id.buttonCancel);
        ImageButton imageButton = (ImageButton) rootView.findViewById(R.id.imageButtonEditDate);
        btnSave.setOnClickListener(buttonsAction);
        btnCancel.setOnClickListener(buttonsAction);
        imageButton.setOnClickListener(buttonsAction);
        initRectangleButton(btnSave, EButtonTypes.YELLOW_LIGHT);
        initRectangleButton(btnCancel, EButtonTypes.GRAY);
        initOvalButton(imageButton);
        etDescription = (EditText) rootView.findViewById(R.id.editTextHistoryItemDescription);
        etValue = (EditText) rootView.findViewById(R.id.editTextValue);
        TextInputLayout tilDescription = (TextInputLayout)rootView.findViewById(R.id.textInputLayoutHistoryItemDescription);
        tilValue = (TextInputLayout) rootView.findViewById(R.id.textInputLayoutValue);
        tilDescription.setErrorEnabled(true);
        tilValue.setErrorEnabled(true);
        Spinner spTypes = (Spinner) rootView.findViewById(R.id.spinnerType);
        types = new ArrayList<>(Arrays.asList(EDashboardPages.values()));
        types.remove(EDashboardPages.ALL);
        SpinnerHistoryTypesAdapter adapter = new SpinnerHistoryTypesAdapter(getActivity(), R.layout.adapter_item_spinner_history_type, types);
        adapter.setDropDownViewResource(R.layout.adapter_item_spinner_history_type);
        spTypes.setAdapter(adapter);
        spTypes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currentPage = types.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if(currentPage == EDashboardPages.ALL){
            spTypes.setSelection(0);
            currentPage = EDashboardPages.EARNINGS;
        }else{
            spTypes.setSelection(types.indexOf(currentPage));
        }
        tvDate = (TextView) rootView.findViewById(R.id.textViewDate);
        initRectangleButton(tvDate, EButtonTypes.WHITE);
        tvDate.setOnClickListener(buttonsAction);
        if (date == 0){
            date = System.currentTimeMillis()/1000;
        }
        updateDateView();
    }

    private View.OnClickListener buttonsAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.buttonSave:
                    tilValue.setError("");
                    String sValue = etValue.getText().toString();
                    if(TextUtils.isEmpty(sValue)){
                        tilValue.setError(getString(R.string.new_history_item_message_empty_value));
                        return;
                    }
                    double value = Double.parseDouble(sValue);
                    if(value == 0){
                        tilValue.setError(getString(R.string.new_history_item_message_value_equal_zero));
                        return;
                    }
//                    saveNewHistoryItem(value);
                    saveFirstStep();
                    break;
                case R.id.buttonCancel:
                    getFragmentManager().popBackStack();
                    break;
                case R.id.textViewDate:
                case R.id.imageButtonEditDate:
                    final Calendar c = Calendar.getInstance();
                    c.setTimeInMillis(date*1000);
                    int mYear = c.get(Calendar.YEAR);
                    int mMonth = c.get(Calendar.MONTH);
                    int mDay = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                            new DatePickerDialog.OnDateSetListener() {
                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(year, monthOfYear, dayOfMonth);
                                    date =  calendar.getTimeInMillis()/1000;
                                    updateDateView();
                                }
                            }, mYear, mMonth, mDay);
                    dpd.getDatePicker().setCalendarViewShown(false);
                    dpd.show();
                    break;
            }
        }
    };

    private IDBWriteListener writeListener = new IDBWriteListener() {
        @Override
        public void onWriteFinish(long count) {
            Dialogs.hideDialog();
            if(count == 0){
                Dialogs.showCustomOneButtonDialog(getActivity(), R.string.btn_label_ok, R.string.new_history_item_dialog_message_item_was_not_created, null, true);
            }else{
                Dialogs.showCustomOneButtonDialog(getActivity(), R.string.btn_label_ok, R.string.new_history_item_dialog_message_item_created_successfully, new IOneButtonDialogCallback() {
                    @Override
                    public void onButtonClick() {
                        getFragmentManager().popBackStack();
                    }

                    @Override
                    public void onCancel() {
                        getFragmentManager().popBackStack();
                    }
                }, true);
            }
        }
    };

    private void updateDateView(){
        tvDate.setText(Utils.millisecondsToDate(date * 1000, "dd MMM yyyy"));
    }

    private void saveFirstStep(){
        Dialogs.showLoadingProgressDialog(getActivity());
        getDBHelper().asyncGetHistoryMap(EDashboardPages.ALL, getSelectedBalanceItem(), new IDBReadListener<List<HistoryItem>>() {
            @Override
            public void onReadFinish(List<List<HistoryItem>> items) {
            }

            @Override
            public void onReadFinish(Map<Long, List<HistoryItem>> items) {
                BalanceItem balanceItem = getSelectedBalanceItem();
                if (items.isEmpty()) {
//                    saveToDB(balanceItem.getValue(), null);
                    //TODO
                } else {
                    TreeMap<Long, List<HistoryItem>> itemsTreeMap = (TreeMap<Long, List<HistoryItem>>) items;
                    if (itemsTreeMap.firstKey() < date) {
                        HistoryItem first = getHeadOfList(itemsTreeMap.get(itemsTreeMap.firstKey()));
                        HistoryItem newItem = getHistoryItem(first, null);
                        assert first != null;
                        first.setConnectedItemKeyNext(newItem.getKey());
                        //TODO save to DB
                        return;
                    }
                    if (itemsTreeMap.lastKey() > date) {
                        HistoryItem last = getTailOfList(itemsTreeMap.get(itemsTreeMap.lastKey()));
                        HistoryItem newItem = getHistoryItem(null, last);
                        assert last != null;
                        last.setConnectedItemKeyNext(newItem.getKey());
                        //TODO update all items after this one
                        return;
                    }
                    List<HistoryItem> historyItems;
                    if (!items.containsKey(date)) {
                        SortedMap<Long, List<HistoryItem>> subMap = itemsTreeMap.subMap(itemsTreeMap.firstKey(), date);
                        historyItems = subMap.get(subMap.lastKey());
                    } else {
                        historyItems = items.get(date);
                    }
                }
            }
        });
    }

    /*private void saveToDB(double balanceValue, HistoryItem itemToBind){
        double value = Double.parseDouble(etValue.getText().toString());
        if(currentPage == EDashboardPages.COSTS || currentPage == EDashboardPages.PLANNED_COSTS){
            value = -value;
        }
        BalanceItem balanceItem = getSelectedBalanceItem();
        long currentTime = System.currentTimeMillis();
        HistoryItem historyItem = new HistoryItem();
        String description = etDescription.getText().toString();
        if(!TextUtils.isEmpty(description)){
            historyItem.setDescription(description);
        }
        historyItem.setBalanceId(balanceItem.getId());
        historyItem.setTypeId(currentPage.getId());
        historyItem.setCreatedAt(currentTime / 1000);
        historyItem.setDate(date);
        historyItem.setValue(value);
        historyItem.setCreatedByUser("Local");//TODO
        historyItem.setUpdatedByUser(historyItem.getCreatedByUser());
        historyItem.setKey(currentTime);
        historyItem.setBalanceValueBefore(balanceValue);
        historyItem.setBalanceValueAfter(balanceValue + value);
        if(itemToBind != null)
            historyItem.setConnectedItemKeyPrev(itemToBind.getKey());
        balanceItem.setUpdatedAt(currentTime/1000);
        balanceItem.setUpdatedByUser(historyItem.getCreatedByUser());
        balanceItem.setValue(balanceItem.getValue() + value);
        //TODO to DB
    }*/

    private void updateItemsInDB(){

    }

    private HistoryItem getHeadOfList(List<HistoryItem> items){
        for(HistoryItem historyItem : items){
            if(historyItem.getConnectedItemKeyNext() == 0)
                return historyItem;
        }
        return null;
    }

    private HistoryItem getTailOfList(List<HistoryItem> items){
        for(HistoryItem historyItem : items){
            if(historyItem.getConnectedItemKeyPrev() == 0)
                return historyItem;
        }
        return null;
    }

    private HistoryItem getHistoryItem(HistoryItem connectToPrev, HistoryItem connectToNext){
        double value = Double.parseDouble(etValue.getText().toString());
        if(currentPage == EDashboardPages.COSTS || currentPage == EDashboardPages.PLANNED_COSTS){
            value = -value;
        }
        BalanceItem balanceItem = getSelectedBalanceItem();
        long currentTime = System.currentTimeMillis();
        HistoryItem historyItem = new HistoryItem();
        String description = etDescription.getText().toString();
        if(!TextUtils.isEmpty(description)){
            historyItem.setDescription(description);
        }
        historyItem.setBalanceId(balanceItem.getId());
        historyItem.setTypeId(currentPage.getId());
        historyItem.setCreatedAt(currentTime / 1000);
        historyItem.setDate(date);
        historyItem.setValue(value);
        historyItem.setCreatedByUser("Local");//TODO
        historyItem.setUpdatedByUser(historyItem.getCreatedByUser());
        historyItem.setKey(currentTime);
        historyItem.setConnectedItemKeyNext(connectToNext == null ? 0 : connectToNext.getKey());
        historyItem.setConnectedItemKeyPrev(connectToNext == null ? 0 : connectToNext.getKey());
        if(connectToPrev == null && connectToNext!= null){
            historyItem.setBalanceValueBefore(connectToNext.getBalanceValueBefore());
            connectToNext.setBalanceValueBefore(historyItem.getBalanceValueAfter());
            connectToNext.setBalanceValueAfter(connectToNext.getBalanceValueBefore() + connectToNext.getValue());
        }else if(connectToPrev != null){
            historyItem.setBalanceValueBefore(connectToPrev.getBalanceValueAfter());
        }
        historyItem.setBalanceValueAfter(historyItem.getBalanceValueBefore() + value);
        //TODO update balance
        return historyItem;
    }

}
