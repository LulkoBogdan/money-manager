package com.bogdan.lulko.moneymanager.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.interfaces.IOneButtonDialogCallback;
import com.bogdan.lulko.moneymanager.interfaces.ITwoButtonDialogCallback;

public class Dialogs {

    private static AlertDialog alertDialog;

    public static void showCustomTwoButtonDialog(Context context, int labelButtonLeft, int labelButtonRight, String message, final ITwoButtonDialogCallback callback, boolean cancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
        builder.setTitle("");
        builder.setView(getDialogView(message, context));
        builder.setCancelable(false);
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, context.getResources().getString(labelButtonLeft), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onLeftButtonClick();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(labelButtonRight), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onRightButtonClick();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface arg0) {
                if (callback != null)
                    callback.onCancel();
            }
        });
        dialog.show();
        paintButtons(dialog.getButton(AlertDialog.BUTTON_POSITIVE), dialog.getButton(AlertDialog.BUTTON_NEUTRAL), context);
    }

    public static void showCustomTwoButtonDialog(Context context, int labelButtonLeft, int labelButtonRight, int message, final ITwoButtonDialogCallback callback, boolean cancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
        builder.setTitle("");
        builder.setView(getDialogView(context.getResources().getString(message), context));
        builder.setCancelable(cancelable);
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, context.getResources().getString(labelButtonLeft), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onLeftButtonClick();
            }
        });
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getResources().getString(labelButtonRight), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onRightButtonClick();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

            @Override
            public void onCancel(DialogInterface arg0) {
                if (callback != null)
                    callback.onCancel();
            }
        });
        dialog.show();
        paintButtons(dialog.getButton(AlertDialog.BUTTON_POSITIVE), dialog.getButton(AlertDialog.BUTTON_NEUTRAL), context);
    }

    public static void showCustomOneButtonDialog(Context context, int buttonLabel, int message, final IOneButtonDialogCallback callback, boolean cancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
        builder.setTitle("");
        builder.setView(getDialogView(context.getResources().getString(message), context));
        builder.setCancelable(cancelable);
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, context.getResources().getString(buttonLabel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onButtonClick();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface arg0) {
                if (callback != null)
                    callback.onCancel();
            }
        });
        dialog.show();
        paintButtons(dialog.getButton(AlertDialog.BUTTON_POSITIVE), dialog.getButton(AlertDialog.BUTTON_NEUTRAL), context);
    }

    public static void showCustomOneButtonDialog(Context context, int buttonLabel, String message, final IOneButtonDialogCallback callback, final boolean cancelable){
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogStyle);
        builder.setTitle("");
        builder.setView(getDialogView(message, context));
        builder.setCancelable(cancelable);
        final AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(cancelable);
        dialog.setButton(DialogInterface.BUTTON_NEUTRAL, context.getResources().getString(buttonLabel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int arg1) {
                dialog.dismiss();
                if (callback != null)
                    callback.onButtonClick();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface arg0) {
                callback.onCancel();
            }
        });
        dialog.show();
        paintButtons(dialog.getButton(AlertDialog.BUTTON_POSITIVE), dialog.getButton(AlertDialog.BUTTON_NEUTRAL), context);
    }

    public static void showProgressDialog(String message, Activity context) {
        hideDialog();
        LinearLayout llDialog = new LinearLayout(context);
        LayoutInflater inflater = context.getLayoutInflater();
        llDialog = (LinearLayout) inflater.inflate(R.layout.dialog_progress_view, llDialog, false);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        ((TextView)llDialog.findViewById(R.id.tvMessage)).setText(message);
        builder.setView(llDialog);
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog = builder.create();
                alertDialog.show();
            }
        });
    }

    public static void hideDialog(){
        if(alertDialog != null)
            alertDialog.dismiss();
    }

    public static void showLoadingProgressDialog(Activity context) {
        showProgressDialog(context.getString(R.string.dialog_title_please_wait),  context);
    }

    private static RelativeLayout getDialogView(String message, Context context){
        RelativeLayout rlDialog = new RelativeLayout(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        rlDialog = (RelativeLayout) inflater.inflate(R.layout.dialog_view, rlDialog, false);
        ((TextView)rlDialog.findViewById(R.id.tvMessage)).setText(message);
        return rlDialog;
    }

    private static void paintButtons(Button buttonPositive, Button buttonNeutral, Context context) {
        if (buttonPositive != null) {
            buttonPositive.setBackgroundResource(R.drawable.dialog_button);
            buttonPositive.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));
        }
        if (buttonNeutral != null) {
            buttonNeutral.setBackgroundResource(R.drawable.dialog_button);
            buttonNeutral.setTextColor(ContextCompat.getColor(context, R.color.textColorPrimary));
        }
    }
}
