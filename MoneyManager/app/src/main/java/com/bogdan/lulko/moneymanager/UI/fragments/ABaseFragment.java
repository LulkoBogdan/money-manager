package com.bogdan.lulko.moneymanager.UI.fragments;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.UI.activities.ABaseActivity;
import com.bogdan.lulko.moneymanager.UI.activities.MainActivity;
import com.bogdan.lulko.moneymanager.enums.EButtonTypes;
import com.bogdan.lulko.moneymanager.helpers.DBHelper;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

import codetail.graphics.drawables.DrawableHotspotTouch;
import codetail.graphics.drawables.LollipopDrawable;
import codetail.graphics.drawables.LollipopDrawablesCompat;

public abstract class ABaseFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyBoard();
    }

    public Drawable getDrawableRipple(int id){
        return LollipopDrawablesCompat.getDrawable(getResources(), id, getActivity().getTheme());
    }

    public void initRectangleButton(View button, EButtonTypes buttonType){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return;
        int resId = R.drawable.ripple_rectangle_yellow_light;
        if(buttonType == EButtonTypes.YELLOW_LIGHT)
            resId = R.drawable.ripple_rectangle_yellow_light;
        else if(buttonType == EButtonTypes.GRAY)
            resId = R.drawable.ripple_rectangle_gray;
        else if(buttonType == EButtonTypes.WHITE)
            resId = R.drawable.ripple_rectangle_white;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            button.setBackground(getDrawableRipple(resId));
        }else{
            button.setBackgroundDrawable(getDrawableRipple(resId));
        }
        button.setClickable(true);
        button.setOnTouchListener(new DrawableHotspotTouch((LollipopDrawable) button.getBackground()));
    }

    public void initOvalButton(View button){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            return;
        int resId = R.drawable.ripple_oval_white;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN){
            button.setBackground(getDrawableRipple(resId));
        }else{
            button.setBackgroundDrawable(getDrawableRipple(resId));
        }
        button.setClickable(true);
        button.setOnTouchListener(new DrawableHotspotTouch((LollipopDrawable) button.getBackground()));
    }

    public void changeFragment(Fragment fragment, boolean popToBackStack, boolean clearBackStack, boolean withAnimation){
        ((ABaseActivity)getActivity()).changeFragment(fragment, popToBackStack, clearBackStack, withAnimation);
    }

    public void hideKeyBoard(){
        ((ABaseActivity)getActivity()).hideKeyBoard(getActivity().getWindow().getDecorView().getWindowToken());
    }

    public BalanceItem getSelectedBalanceItem(){
        return ((MainActivity)getActivity()).getSelectedBalanceItem();
    }

    public void setSelectedBalanceItem(int id){
        ((MainActivity) getActivity()).setSelectedBalance(id);
    }

    public void setSelectedBalanceItem(BalanceItem selectedBalanceItem){
        ((MainActivity) getActivity()).setSelectedBalance(selectedBalanceItem);
    }

    public DBHelper getDBHelper(){
        DBHelper dbHelper = null;
        if(getActivity() != null && isAdded()){
            dbHelper = ((MainActivity)getActivity()).getDbHelper();
        }
        return dbHelper;
    }

    public void openDrawer(){
        if(getActivity() != null){
            ((MainActivity)getActivity()).openDrawer();
        }
    }

    public void closeDrawer(){
        if(getActivity() != null){
            ((MainActivity)getActivity()).closeDrawer();
        }
    }

    public void setSelectedDrawerPosition(int position){
        if(getActivity() != null)
        ((MainActivity)getActivity()).setSelectedDrawerNavigationItem(position);
    }

    protected Toolbar initToolbar(int toolbarViewId, int titleId, int iconResId, View rootView, boolean setDisplayHomeAsUpEnabled){
        Toolbar toolbar = (Toolbar) rootView.findViewById(toolbarViewId);
        toolbar.setTitleTextColor(ContextCompat.getColor(getActivity(), R.color.textColorPrimary));
        if(titleId > 0)
            toolbar.setTitle(titleId);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        if(iconResId > 0) {
            final ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionBar != null) {
                actionBar.setHomeAsUpIndicator(iconResId);
                actionBar.setDisplayHomeAsUpEnabled(setDisplayHomeAsUpEnabled);
            }
        }
        return toolbar;
    }

}
