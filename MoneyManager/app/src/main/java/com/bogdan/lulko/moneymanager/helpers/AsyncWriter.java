package com.bogdan.lulko.moneymanager.helpers;

import android.os.AsyncTask;

import com.bogdan.lulko.moneymanager.interfaces.IDBWriteListener;

public abstract class AsyncWriter extends AsyncTask<Void, Void, Long>{

    private IDBWriteListener writeListener;

    public AsyncWriter(IDBWriteListener writeListener){
        this.writeListener = writeListener;
    }

    @Override
    protected void onPostExecute(Long aLong) {
        super.onPostExecute(aLong);
        if(writeListener != null)
            writeListener.onWriteFinish(aLong);
    }
}
