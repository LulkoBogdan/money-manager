package com.bogdan.lulko.moneymanager.models;

import android.content.Context;

import com.bogdan.lulko.moneymanager.R;

public class BalanceCurrency {

    private String currencyCode;
    private String currencyName;
    private String currencySymbol;
    private String countryCode;
    private String countryName;

    public BalanceCurrency() {
    }

    public BalanceCurrency(String countryName) {
        this.countryName = countryName;
        this.currencyCode = this.currencyName = this.currencySymbol = this.countryCode = "";
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public static String NONE(Context context){
        return context.getString(R.string.label_none);
    }
}
