package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.models.BalanceItem;
//TODO maybe not needed. Try to make animation on info page - when user click "Edit" button TextView's become EditText's and appears action button save/cancel
public class EditBalanceFragment extends ABaseFragment {

    private BalanceItem balanceItem;

    public static EditBalanceFragment getInstance(BalanceItem balanceItem){
        EditBalanceFragment fragment = new EditBalanceFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.BundleKeys.KEY_BALANCE_ITEM_TO_EDIT, balanceItem);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDataFromArguments();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getDataFromArguments(){
        if(getArguments() != null){
            balanceItem = getArguments().getParcelable(Constants.BundleKeys.KEY_BALANCE_ITEM_TO_EDIT);
        }
    }
}
