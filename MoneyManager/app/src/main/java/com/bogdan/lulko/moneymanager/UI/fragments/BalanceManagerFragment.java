package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bogdan.lulko.moneymanager.R;
import com.bogdan.lulko.moneymanager.adapters.BalancesAdapter;
import com.bogdan.lulko.moneymanager.helpers.Constants;
import com.bogdan.lulko.moneymanager.helpers.Dialogs;
import com.bogdan.lulko.moneymanager.interfaces.IDBReadListener;
import com.bogdan.lulko.moneymanager.models.BalanceItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BalanceManagerFragment extends ABaseFragment{

    private List<BalanceItem> balanceItems = new ArrayList<>();
    private BalancesAdapter adapter;

    public static BalanceManagerFragment getInstance(boolean goToCreateNewBalance){
        BalanceManagerFragment fragment = new BalanceManagerFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.BundleKeys.KEY_GO_TO_NEW, goToCreateNewBalance);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_balance_manager, container, false);
        initView(rootView);
        setSelectedDrawerPosition(Constants.NavigationDrawer.ITEM_MANAGE_BALANCES_POSITION);
        getDataFromArguments();
        readAllBalances();
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                openDrawer();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView(View rootView){
        initToolbar(R.id.toolbar, R.string.balance_manager_toolbar_title, R.mipmap.ic_menu_black_36dp, rootView, true);
        RecyclerView rvBalances = (RecyclerView) rootView.findViewById(R.id.recyclerViewBalances);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvBalances.setLayoutManager(linearLayoutManager);
        FloatingActionButton fabPlus = (FloatingActionButton) rootView.findViewById(R.id.fabPlus);
        fabPlus.setOnClickListener(fabActionListener);
        adapter = new BalancesAdapter(balanceItems, getActivity());
        rvBalances.setAdapter(adapter);
        setSelectedDrawerPosition(Constants.NavigationDrawer.ITEM_HOME_POSITION);
     }

    private void getDataFromArguments(){
        Bundle arguments = getArguments();
        boolean goToNew = false;
        if(arguments != null){
            goToNew = arguments.getBoolean(Constants.BundleKeys.KEY_GO_TO_NEW, false);
        }
        if(goToNew){
            changeFragment(new AddNewBalanceFragment(), true, false, true);
            arguments.putBoolean(Constants.BundleKeys.KEY_GO_TO_NEW, false);
        }
    }

    private void readAllBalances(){
        Dialogs.showLoadingProgressDialog(getActivity());
        balanceItems.clear();
        getDBHelper().asyncGetAllBalances(new IDBReadListener<BalanceItem>() {
            @Override
            public void onReadFinish(List<BalanceItem> items) {
                balanceItems.addAll(items);
                if(adapter != null)
                    adapter.notifyDataSetChanged();
                Dialogs.hideDialog();
            }
            @Override
            public void onReadFinish(Map<Long, BalanceItem> items) {}
        });
    }

    private View.OnClickListener fabActionListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeFragment(new AddNewBalanceFragment(), true, false, true);
        }
    };
}
