package com.bogdan.lulko.moneymanager.UI.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bogdan.lulko.moneymanager.enums.EDashboardPages;
import com.bogdan.lulko.moneymanager.helpers.Constants;

public class ChartFragment extends ABaseFragment{
//TODO add view pager with 2 fragments. One with chart of balance change with possibility to change period (from month to year). Second with pie chart of user balance use with possibility to change period and type(earnings or costs).Lat one available only if balance is shared on few users
    private EDashboardPages currentPage = EDashboardPages.ALL;

    public static ChartFragment getInstance(EDashboardPages currentPage){
        ChartFragment fragment = new ChartFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.BundleKeys.KEY_PAGE_TYPE_ID, currentPage.getId());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
